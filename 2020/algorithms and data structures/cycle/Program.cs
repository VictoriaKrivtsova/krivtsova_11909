﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cycle
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            Cycle(n);
        }
        public static int Cycle(int n)
        {
            int a = 0;
            int b = 1;
            int c = 0;

            if (n == 0) return 0;
            if (n == 1) return 1;

            for (int i = 2; i < n; i++)
            {
                c = a + b;
                a = b;
                b = c;
            }
            return c;
        }
    }
}
