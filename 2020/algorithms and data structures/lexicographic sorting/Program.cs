﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lexicographic_sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter words: ");
            string str = Console.ReadLine();//строка со словами
            string[] words = str.Split(' ');//массив со словами
            var countOfWords = words.Length;

            Console.WriteLine("Enter the symbols: ");
            var symbols = Console.ReadLine().Split(' ');//алфавит
            var countOfSymbols = symbols.Length;//количество используемых символов

            var length = words[0].Length;//длина слов(сделано для одинаковой длины)
            var dictionaryPocket = new Dictionary<string, Queue<string>> ();//карман, реализованный в виде словаря

            for (int symbol = 0; symbol < countOfSymbols; symbol++)//проверяем какому символу соответсвует разряд
            {
                if (!dictionaryPocket.ContainsKey(symbols[symbol]))
                {
                    dictionaryPocket[symbols[symbol]] = new Queue<string>();
                }
            }
            //цикл, в котором будет происходить поразрядная сортировка
            for (int i = length-1; i >= 0; i--)//проходим по всем разрядам(длина нашего слова)
            {
                for (int word = 0; word < countOfWords; word++)//проходим по всем словам
                {
                    for (int symbol = 0; symbol < countOfSymbols; symbol++)//проверяем какому символу соответсвует разряд
                    {
                        if (words[word][i].ToString() == symbols[symbol])//если разряд данного слова это данный символ, то..
                        {
                            //if (!dictionaryPocket.ContainsKey(symbols[symbol]))
                            //{
                            //    dictionaryPocket[symbols[symbol]] = null;
                            //}
                            dictionaryPocket[symbols[symbol]].Enqueue(words[word]);//добавляем слово в словарь(в очередь)
                            break;
                        }
                    }
                }
                var count = 0;
                for (int symbol = 0; symbol < countOfSymbols; symbol++)
                {
                    while (dictionaryPocket[symbols[symbol]].Count != 0 && count != words.Length)
                    {
                        words[count] = dictionaryPocket[symbols[symbol]].Dequeue();
                        count++;
                    }
                }
            }

            for (int i = 0; i < countOfWords; i++)
            {
                Console.WriteLine(words[i]);
            }

            Console.ReadKey();
        }
    }
}
