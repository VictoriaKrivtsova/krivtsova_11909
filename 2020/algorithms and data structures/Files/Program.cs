﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Files
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<string> text = new Stack<string>();
            FileStream file1 = new FileStream("SomeText.txt", FileMode.Open);
            StreamReader reader = new StreamReader(file1);
            StreamWriter writer = new StreamWriter("NewText.txt");
            string StringLine = "";
                while (!reader.EndOfStream)
                {
                    StringLine = reader.ReadLine();
                    text.Push(StringLine);
                }
            reader.Close();

                while (text.Count != 0)
                {
                    writer.WriteLine(text.Pop());
                }
            writer.Close();
        }
    }
}
