﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class LinkedListNode
    {
        public Object Value { get; set; }

        public LinkedListNode Next { get; set; }

    }

    public class LinkedList
    {
        public LinkedListNode First { get; set; }
        public LinkedListNode Last { get; set; }

      
        public  void AddToLinkedList(object _value)
        {
            LinkedListNode node = new LinkedListNode();
            node.Value = _value;

            if (First == null)
            {
                First = node;
                Last = node;
            }
            else
            {
                Last.Next = node;
                Last = node;
            }
        }

        
        public void DisplayAllItems()
        {
            LinkedListNode current = First;
            while (current != null)
            {
                Console.WriteLine(current.Value);
                current = current.Next;
            }
        }
    }

    class Program
    {
        public static void Main(string[] args)
        {
            LinkedList singlyLinkedList = new LinkedList();
            singlyLinkedList.AddToLinkedList(4);
            singlyLinkedList.AddToLinkedList(5);
            singlyLinkedList.AddToLinkedList(7);
            singlyLinkedList.AddToLinkedList(2);
            singlyLinkedList.AddToLinkedList(1);
            singlyLinkedList.AddToLinkedList(10);

            Console.ReadLine();
        }

        public static bool hasCycle(LinkedListNode first)
        {
            LinkedListNode FirstPoint = first;
            LinkedListNode SecondPoint = first;

            while (SecondPoint != null && SecondPoint.Next != null)
            {
                FirstPoint = FirstPoint.Next;
                SecondPoint = SecondPoint.Next.Next;
                if (FirstPoint == SecondPoint)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
        
    

