﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semestrovka
{
    class Conversation
    {
        private String outgoing;//Номер человека, который звонит//
        private String ingoing;//Номер человека, которому звонят//
        private DateTime date;//Дата(дм.мм.гг)//
        private int startTime;//время начала разговора//
        private int endTime;//время окончания разговора//

        //finished conversation(Список закончившихся разговоров)
        public Conversation(String outgoing, String ingoing, DateTime date, int startTime, int endTime)
        {
            this.outgoing = outgoing;
            this.ingoing = ingoing;
            this.date = date;
            this.startTime = startTime;
            this.endTime = endTime;
        }

        //running conversation(Список незаконченных разговоров)
        public Conversation(String outgoing, String ingoing, DateTime date, int startTime)
        {
            this.outgoing = outgoing;
            this.ingoing = ingoing;
            this.date = date;
            this.startTime = startTime;
            this.endTime = 0;
        }

        //Конструкторы//
        public string GetOutgoing()
        {
            return outgoing;
        }

        public void SetOutgoing(string outgoing)
        {
            this.outgoing = outgoing;
        }

        public string GetIngoing()
        {
            return ingoing;
        }

        public void SetIngoing(String ingoing)
        {
            this.ingoing = ingoing;
        }

        public DateTime GetDate()
        {
            return date;
        }

        public void SetDate(DateTime date)
        {
            this.date = date;
        }

        public int GetStartTime()
        {
            return startTime;
        }

        public void SetStartTime(int startTime)
        {
            this.startTime = startTime;
        }

        public int GetEndTime()
        {
            return endTime;
        }

        public void SetEndTime(int endTime)
        {
            this.endTime = endTime;
        }

        public override string ToString()//проверка:закончен ли разговор//
        {
            return this.outgoing + " " + this.ingoing + " " + String.Format("{0:dd.MM.yyyy}", this.date) + " " + this.startTime + " " + this.endTime;
        }

        public override bool Equals(Object obj)//Проверка на совпадение//
        {
            if (obj is Conversation)
            {
                Conversation conv = (Conversation)obj;
                return this.outgoing.Equals(conv.GetOutgoing())
                        && this.ingoing.Equals(conv.GetIngoing())
                        && this.date.Equals(conv.GetDate())
                        && this.startTime == conv.GetStartTime();
            }
            return false;
        }
    }
}
