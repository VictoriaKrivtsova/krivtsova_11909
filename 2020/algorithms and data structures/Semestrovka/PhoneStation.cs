﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace Semestrovka
{
    class PhoneStation
    {
        private static readonly PhonesList RUNNING = new PhonesList();
        private static readonly PhonesList FINISHED = new PhonesList();
        private static readonly PhonesList LIMIT_OVERFLOWED = new PhonesList();
        private static readonly PhonesList ARCHIVE = new PhonesList();
        private static  FileStream CONVERSATIONS_FILE = new FileStream("C:\\Users\\Виктория\\OneDrive\\Desktop\\Semestrovka\\Conversation.txt", FileMode.Open);

        
        private const long limit = 1000;
        static void Main(string[] args)
       {
            using (StreamReader br = new StreamReader("C:\\Users\\Виктория\\OneDrive\\Desktop\\Semestrovka\\Hello_Message..txt"))
            {
                Console.WriteLine(br.ReadToEnd());
                String command = "";

                command = Console.ReadLine();
                while (!command.Equals("stop"))
                {
                    DoCommand(command);
                    Console.WriteLine("Enter your command below:");
                    command = Console.ReadLine();
                }
                Console.WriteLine("See you soon, User!");
            }
        }

        private static void DoCommand(String command)
        //Кейс для введенных комманд
        {
            switch (command)
            {
                case "encode":
                    {
                        Encode(CONVERSATIONS_FILE, RUNNING, FINISHED);
                        Console.WriteLine("List of conversations from file\n" + RUNNING.ToString() + FINISHED.ToString());
                        Console.WriteLine("Encode done");
                        break;
                    }
                case "decode":
                    {
                        Decode(CONVERSATIONS_FILE, RUNNING, FINISHED);
                        Console.WriteLine("Decode done");
                        break;
                    }
                case "add":
                    {
                        Console.WriteLine("Enter data in this format:\nOUTGOING_NUMBER INGOMING_NUMBER DATE START_TIME");
                        String newConversationString = Console.ReadLine();
                        try
                        {
                            Conversation newConversation = СreateConvFromStrings(newConversationString.Split(' '));
                            if (
                                    RUNNING.HasConversation(newConversation.GetIngoing()) ||
                                            RUNNING.HasConversation(newConversation.GetOutgoing())
                            )
                                Console.WriteLine("Abonent is already talking");
                            else
                            {
                                if (FINISHED.GetConversationByNumbers(newConversation.GetOutgoing(), newConversation.GetIngoing()) != null)
                                {
                                    Update(newConversation, RUNNING, FINISHED);
                                    Console.WriteLine("Update done");
                                }
                                else
                                {
                                    Add(newConversation, RUNNING);
                                    Console.WriteLine("Adding done");
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    }
                case "delete":
                    {
                        Console.WriteLine("Enter data in this format:\nNUMBER DurationOfCall");
                        String deleteConversationString = Console.ReadLine();
                        if (RUNNING.HasConversation(deleteConversationString.Split(' ')[0]))
                        {
                            //TODO: избавиться от .getConversation(номер)
                            Conversation deleteConversation = RUNNING.GetConversation(deleteConversationString.Split(' ')[0]);
                            Delete(deleteConversation, RUNNING, FINISHED, Convert.ToInt32(deleteConversationString.Split(' ')[1]));
                            Console.WriteLine("Delete done");
                        }
                        else
                            Console.WriteLine("Abonent isn't talking now");
                        break;
                    }
                case "overflow":
                    {
                        CreateOverflow(LIMIT_OVERFLOWED, RUNNING, FINISHED);
                        Console.WriteLine("List of conversations which exceeded the conversation limit:\n" + LIMIT_OVERFLOWED.ToString());
                        break;
                    }
                case "archive":
                    {
                        Console.WriteLine("Enter some time moment:");
                        long moment = Convert.ToInt64(Console.ReadLine());
                        CreateArchive(moment, FINISHED, ARCHIVE);
                        Console.WriteLine("Archive list:\n" + ARCHIVE.ToString());
                        break;
                    }
                case "bigAbonent":
                    {
                        Console.WriteLine(FindMostChattyAbonent(ARCHIVE));
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Incorrect command entered");
                        break;
                    }
            }
        }

        private static void Encode(FileStream file, PhonesList running, PhonesList finished)
        //Распределяем разговоры по спискам закончились/не закончились//КОДИРОВАНИЕ//
        {
            using (StreamReader br = new StreamReader(file))
            {
                try
                {
                    String line = br.ReadLine(); ;
                    while (line != null)
                    {
                        String[] strings = line.Split(' ');
                        Conversation conversation = СreateConvFromStrings(strings);
                        if (conversation.GetEndTime() == 0)
                            running.AddConversation(conversation);
                        else finished.AddConversation(conversation);
                        line = br.ReadLine();
                    }
                }
                catch (IOException e)
                {
                    e.ToString();
                }
            }
        }

        private static void Decode(FileStream file, PhonesList running, PhonesList finished)
        //выписываем в файл сначала список running, а потом finishing//ДЕКОДИРОВАНИЕ//
        {
            using (StreamWriter fw = new StreamWriter("C:\\Users\\Виктория\\OneDrive\\Desktop\\Semestrovka\\Decode.txt"))
            {
                try
                {
                    for (int i = 0; i < running.GetSize(); i++)
                    {
                        fw.Write(running
                        .GetConversations()[i]
                        .ToString() + "\n");
                    }
                    for (int i = 0; i < finished.GetSize(); i++)
                    {
                        fw.Write(finished
                        .GetConversations()[i]
                        .ToString() + "\n");
                    }
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private static bool Add(Conversation conversation, PhonesList list)
        //Добавляем разговор//ДОБАВИТЬ//
        {
            list.AddConversation(conversation);
            return true;
        }

        private static bool Delete(Conversation conversation, PhonesList running, PhonesList finished, int duration)
        //Удаляем разговор//УДАЛИТЬ//
        {
            running.DeleteConversation(conversation);
            Conversation finishedConversation = new Conversation(conversation.GetOutgoing(), conversation.GetIngoing(), conversation.GetDate(), conversation.GetStartTime(), (conversation.GetStartTime()) + duration);
            finished.AddConversation(finishedConversation);
            return true;
        }

        private static void CreateOverflow(PhonesList limitOverflowed, PhonesList running, PhonesList finished)
        //Разговоры, длина которых превысила лимит
        {
            Conversation[] conversations = running.GetConversations();
            foreach (Conversation conversation in conversations)
            {
                if (Convert.ToInt32(DateTime.Now) - conversation.GetStartTime() > limit)
                    limitOverflowed.AddConversation(conversation);
            }
            conversations = finished.GetConversations();
            foreach (Conversation conversation in conversations)
            {
                if (conversation.GetEndTime() - conversation.GetStartTime() > limit)
                    limitOverflowed.AddConversation(conversation);
            }
        }

        private static void Update(Conversation conversation, PhonesList running, PhonesList finished)
        //Обновление списка

        {
            Conversation oldConversation = finished.GetConversationByNumbers(conversation.GetOutgoing(), conversation.GetIngoing());
            finished.DeleteConversation(oldConversation);
            running.AddConversation(conversation);
        }

        private static void CreateArchive(long moment, PhonesList finished, PhonesList archive)
        //Создаем архивный список
        {
            foreach (Conversation conversation in finished.GetConversations())
            {
                if (conversation.GetEndTime() > moment)
                {
                    archive.AddConversation(conversation);
                    finished.DeleteConversation(conversation);
                }
            }
        }

        private static String FindMostChattyAbonent(PhonesList archive)
        //Наиболее часто встречающиймя абонент
        {
            Dictionary<string, int> abonents = new Dictionary<string, int>();
            foreach (Conversation conversation in archive.GetConversations())
            {
                String outgoing = conversation.GetOutgoing();
                String ingoing = conversation.GetIngoing();
                if (abonents.ContainsKey(outgoing))
                {
                    abonents[outgoing]++;
                }
                else
                {
                    abonents[outgoing] = 1;
                }

                if (abonents.ContainsKey(ingoing))
                {
                    abonents[ingoing]++;
                }
                else
                {
                    abonents[ingoing] = 1;
                }
            }

            int max = 0;
            String mostChattyAbonent = "";
            foreach (var entry in abonents)
            {
                int number = entry.Value;
                if (number > max)
                {
                    mostChattyAbonent = entry.Key;
                    max = number;
                }
            }
            return mostChattyAbonent;
        }

        private static Conversation СreateConvFromStrings(String[] strings)
        //Из массива строк создаем разговор //
        {
            Conversation conversation;
            if (strings.Length == 4)
            {
                try
                {
                    DateTime date = DateTime.ParseExact(strings[2], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    conversation = new Conversation(strings[0], strings[1], date, int.Parse(strings[3]));
                    return conversation;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            if (strings.Length == 5)
            {
                try
                {
                    DateTime date = DateTime.ParseExact(strings[2], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    conversation = new Conversation(strings[0], strings[1], date, int.Parse(strings[3]), int.Parse(strings[4]));
                    return conversation;
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            throw new Exception("Can't create conversation from such data");
        }
    }
}
