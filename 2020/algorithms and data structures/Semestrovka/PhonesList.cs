﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semestrovka
{
    public class PhonesList
    {
        public PhonesList()
        {
            this.conversations = new Conversation[0];
        }
        private Conversation[] conversations;
        //Конструктор//
        public PhonesList(Conversation[] conversations) { this.conversations = conversations; }

        //конструкторы
        public Conversation[] GetConversations() { return conversations; }

        public void SetConversations(Conversation[] conversations)
        {
            this.conversations = conversations;
        }

        public bool HasConversation(String number)//Проверяем, имеется ли в нашем списке такой номер
        {
            foreach (Conversation conversation in conversations)
            {
                if (conversation.GetOutgoing().Equals(number) || conversation.GetIngoing().Equals(number))
                    return true;
            }
            return false;
        }

        public Conversation GetConversation(String number)//Поиск текущего номера в разговорах//
        {
            foreach (Conversation conversation in conversations)
            {
                if (conversation.GetOutgoing().Equals(number) || conversation.GetIngoing().Equals(number))
                    return conversation;
            }
            return null;
        }

        public Conversation GetConversationByNumbers(String outgoing, String ingoing)
        {
            foreach (Conversation conversation in conversations)
            {
                if (conversation.GetOutgoing().Equals(outgoing) && conversation.GetIngoing().Equals(ingoing))
                {
                    return conversation;
                }
            }
            return null;
        }

        public void AddConversation(Conversation conversation)//Добавление нового разговора//
        {
            Array.Resize(ref conversations, conversations.Length + 1);
            conversations[conversations.Length - 1] = conversation;
        }

        public bool DeleteConversation(Conversation conversation)//Удаляем выбранный разговор//
        {
            for (int i = 0; i < conversations.Length; i++)
            {
                if (conversations[i].Equals(conversation))
                {
                    Conversation[] conversations1 = new Conversation[conversations.Length - 1];
                    Array.Copy(conversations, i + 1, conversations, i, conversations.Length - i - 1);
                    Array.Copy(conversations, 0, conversations1, 0, conversations.Length - 1);
                    conversations = conversations1;
                    return true;
                }
            }
            return false;
        }

        public int GetSize() { return this.conversations.Length; }//размер

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Conversation conversation in conversations)
            {
                sb.Append(conversation.ToString()).Append("\n");
            }
            return sb.ToString();
        }
    }
}
