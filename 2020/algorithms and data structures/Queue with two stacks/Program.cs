﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue_with_two_stacks
{
    class Program
    { 
        public class Queue<E>
        {

            private Stack<E> inbox = new Stack<E>();
            private Stack<E> outbox = new Stack<E>();

            public void queue(E item)
            {
                inbox.Push(item);
            }

            public E dequeue()
            {
                if (outbox.Count!=0)
                {
                    while (inbox.Count!=0)
                    {
                        outbox.Push(inbox.Pop());
                    }
                }
                return outbox.Pop();
            }

        }
    }
}
