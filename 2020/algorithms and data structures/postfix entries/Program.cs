﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace postfix_entries
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<int> Numbers = new Stack<int>();
            var str = Console.ReadLine();
            var text = str.Split(' ');

            for (var i = 0; i < text.Length; i++)
            {
                if (text[i] != "+" && text[i] != "-" && text[i] != "*")
                    Numbers.Push(Convert.ToInt32(text[i]));
                else
                    switch (text[i])
                    {
                        case "+":
                            Numbers.Push(Numbers.Pop() + Numbers.Pop());
                            break;
                        case "*":
                            Numbers.Push(Numbers.Pop() * Numbers.Pop());
                            break;
                        case "-":
                            var vychitaemoe = Numbers.Pop();
                            Numbers.Push(Numbers.Pop() - vychitaemoe);
                            break;
                        default:
                            Console.WriteLine("Ошибка. Неизвестная команда");
                            break;
                    }
            }
            Console.WriteLine(Numbers.Pop());
            Console.ReadKey();
        }
    }
}
