﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Bellman_Ford.Graphs;

namespace Bellman_Ford
{
    public class GraphGenerator
    {
        public const int GraphCount = 100;
        public const int FirstEdges = 100;
        public const int LastEdges = 10000;

        public const int MinWeight = -50;
        public const int MaxWeight = 50;

        public const string DirectoryName = @"Graphs\";
        public const string FileName = @"Graph";
        public const string FileExt = ".txt";

        public void Generate()
        {
            int delta = (LastEdges - FirstEdges) / (GraphCount - 1);
            Random random = new Random();

            List<Graph> graphs = new List<Graph>();
            int current = FirstEdges;
            for (int i = 0; i < GraphCount; i++)
            {
                graphs.Add(GenerateGraph(current, random));
                current += delta;
            }

            WriteGraphs(graphs);
        }

        private List<int[,]> ReadMatrices()
        {
            List<int[,]> matrices = new List<int[,]>();
            for (int i = 0; i < GraphCount; i++)
            {
                using (StreamReader sr = new StreamReader(DirectoryName + FileName + i + FileExt, Encoding.Default))
                {
                    string file = sr.ReadToEnd();
                    string[] lines = file.Trim().Split('\n');

                    int[,] matrix = new int[lines.Length, lines.Length];
                    for (var j = 0; j < lines.Length; j++)
                    {
                        string[] nums = lines[j].Trim().Split();
                        for (var k = 0; k < nums.Length; k++)
                        {
                            matrix[j, k] = int.Parse(nums[k]);
                        }
                    }

                    matrices.Add(matrix);
                }
            }

            return matrices;
        }

        public Graph[] ReadGraphWithArrayEdges()
        {
            return ReadMatrices().Select(Graph.GraphWithArrayEdges).ToArray();
        }

        public Graph[] ReadGraphWithLinkedListEdges()
        {
            return ReadMatrices().Select(Graph.GraphWithLinkedListEdges).ToArray();
        }

        private void WriteGraphs(List<Graph> graphs)
        {
            Directory.CreateDirectory(DirectoryName);
            if (GraphCount != graphs.Count)
            {
                throw new ArgumentException("Incorrect count of graphs");
            }

            for (int i = 0; i < GraphCount; i++)
            {
                using StreamWriter sw = new StreamWriter(DirectoryName + FileName + i + FileExt, false, Encoding.Default);
                for (int j = 0; j < graphs[i].VerticesCount; j++)
                {
                    for (int k = 0; k < graphs[i].VerticesCount; k++)
                    {
                        sw.Write($"{graphs[i].Matrix[j, k]} ");
                    }

                    sw.WriteLine();
                }
            }
        }

        private Graph GenerateGraph(int edgesCount, Random random)
        {
            //Вершин N, ребер примерно N*N/2
            //Ребер M, вершин примерно Sqrt(M*2)
            int verticesCount = (int) Math.Sqrt(edgesCount * 2);
            List<Edge> allEdges = new List<Edge>();
            for (int i = 0; i < verticesCount; i++)
            {
                for (int j = 0; j < verticesCount; j++)
                {
                    if (i != j)
                    {
                        allEdges.Add(new Edge(i, j, RandomFromInterval(random)));
                    }
                }
            }

            List<Edge> edges = new List<Edge>();
            for (int i = 0; i < edgesCount; i++)
            {
                int index = random.Next() % allEdges.Count;
                edges.Add(allEdges[index]);
                allEdges.RemoveAt(index);
            }

            return Graph.GraphFromEdges(verticesCount, edges);
        }

        private int RandomFromInterval(Random random) => random.Next() % (MaxWeight - MinWeight + 1) + MinWeight;
    }
}