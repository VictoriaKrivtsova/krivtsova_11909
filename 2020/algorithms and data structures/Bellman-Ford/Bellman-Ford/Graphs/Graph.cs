﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bellman_Ford.Graphs
{
    public class Graph
    {
        private Graph(int[,] matrix, IEnumerable<Edge> edges)
        {
            if (matrix.GetLength(0) != matrix.GetLength(1))
            {
                throw new ArgumentException("Incorrect matrix");
            }

            Matrix = new int[matrix.GetLength(0),matrix.GetLength(1)];
            Array.Copy(matrix, Matrix, matrix.Length);
            VerticesCount = matrix.GetLength(0);
            Edges = edges;
        }

        private static IEnumerable<Edge> GetEdgeCollection(int[,] matrix)
        {
            List<Edge> edges = new List<Edge>();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] != 0)
                    {
                        edges.Add(new Edge(i, j, matrix[i, j]));
                    }
                }
            }

            return edges;
        }

        private Graph(int verticesCount, List<Edge> edges)
        {
            VerticesCount = verticesCount;
            Edges = edges;
            Matrix = new int[verticesCount, verticesCount];
            foreach (var edge in edges)
            {
                Matrix[edge.From, edge.To] = edge.Weight;
            }
        }

        public static Graph GraphWithArrayEdges(int[,] matrix)
        {
            return new Graph(matrix, GetEdgeCollection(matrix).ToArray());
        }

        public static Graph GraphWithLinkedListEdges(int[,] matrix)
        {
            return new Graph(matrix, new LinkedList<Edge>(GetEdgeCollection(matrix)));
        }

        public static Graph GraphFromEdges(int verticesCount, List<Edge> edges)
        {
            return new Graph(verticesCount, edges);
        }
        
        public int VerticesCount { get; private set; }
        public int[,] Matrix { get; private set; }
        public IEnumerable<Edge> Edges { get; private set; }
    }
}