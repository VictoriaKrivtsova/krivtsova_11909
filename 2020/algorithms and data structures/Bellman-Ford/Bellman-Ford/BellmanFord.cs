﻿using System;
using Bellman_Ford.Graphs;

namespace Bellman_Ford
{
    public class BellmanFord
    {
        public static int[] CalculateBellmanFord(Graph graph, int sourceIndex, out int iterationsCount)
        {
            iterationsCount = 0;
            if (sourceIndex < 0 || sourceIndex >= graph.VerticesCount)
            {
                throw new ArgumentException($"There is no vertex with index {sourceIndex}");
            }

            //Инициализация расстояний
            int[] distances = new int[graph.VerticesCount];
            for (int i = 0; i < graph.VerticesCount; i++)
            {
                distances[i] = int.MaxValue;
            }

            distances[sourceIndex] = 0;

            //Релаксация ребер
            for (int i = 0; i < graph.VerticesCount - 1; i++)
            {
                bool changed = false;
                foreach (var edge in graph.Edges)
                {
                    iterationsCount++;

                    int newCost = distances[edge.From] + edge.Weight;
                    if (distances[edge.From] != int.MaxValue && newCost < distances[edge.To])
                    {
                        changed = true;
                        distances[edge.To] = newCost;
                    }
                }

                if (!changed)
                {
                    break;
                }
            }

            //Проверка на отрицательный цикл 
            foreach (var edge in graph.Edges)
            {
                iterationsCount++;

                if (distances[edge.From] + edge.Weight < distances[edge.To])
                {
                    //throw new InvalidOperationException("Graph has negative edges");
                    return null;
                }
            }

            return distances;
        }
    }
}