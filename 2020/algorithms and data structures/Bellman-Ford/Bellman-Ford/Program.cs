﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Bellman_Ford.Graphs;

namespace Bellman_Ford
{
    class Program
    {
        static void Main(string[] args)
        {
            //Генерация графов
            new GraphGenerator().Generate();
            //Считывание графов
            Graph[] graphsWithArrayEdges = new GraphGenerator().ReadGraphWithArrayEdges();
            Graph[] graphsWithLinkedListEdges = new GraphGenerator().ReadGraphWithLinkedListEdges();
            //Холостой прогон алгоритма для прогрева среды для более корректного результата
            BellmanFord.CalculateBellmanFord(graphsWithArrayEdges[0], 0, out _);
            //Получение результатов сравнения
            int timesCount = 50;
            var arrayEdgesResults = RunAlgorithm(graphsWithArrayEdges, timesCount);
            var linkedListEdgesResults = RunAlgorithm(graphsWithLinkedListEdges, timesCount);
            //Получение количества ребер
            var edgesCounts = graphsWithArrayEdges.Select(x => x.Edges.Count());
            //Запись результатов в файлы
            Directory.CreateDirectory("Results");
            WriteDataInFile(@"Results\ArrayIterations.txt", arrayEdgesResults.iterations);
            WriteDataInFile(@"Results\ArrayTimes.txt", arrayEdgesResults.milliseconds);
            WriteDataInFile(@"Results\LinkedListIterations.txt", linkedListEdgesResults.iterations);
            WriteDataInFile(@"Results\LinkedListTimes.txt", linkedListEdgesResults.milliseconds);
            WriteDataInFile(@"Results\EdgesCounts.txt", edgesCounts);
        }

        public static void WriteDataInFile(string fileName, IEnumerable data)
        {
            using StreamWriter sw = new StreamWriter(fileName, false, Encoding.Default);
            foreach (var obj in data)
            {
                sw.WriteLine(obj);
            }
        }

        public static (int[] iterations, long[] milliseconds) RunAlgorithm(Graph[] graphs, int timeCount)
        {
            int[] iterations = new int[graphs.Length];
            long[] milliseconds = new long[graphs.Length];

            Console.WriteLine("Начало тестирования");
            for (int i = 0; i < graphs.Length; i++)
            {
                BellmanFord.CalculateBellmanFord(graphs[i], 0, out int iterationsCount);
                iterations[i] = iterationsCount;
                
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                //Запускаем несколько раз для получения лучшего времени
                for (int j = 0; j < timeCount; j++)
                {
                    BellmanFord.CalculateBellmanFord(graphs[i], 0, out int _);
                }
                stopwatch.Stop();
                milliseconds[i] = stopwatch.ElapsedMilliseconds;

                Console.WriteLine($"Протестирован {i}-й граф");
            }

            return (iterations, milliseconds);
        }
    }
}