﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList1
{
    class Program
    {
        static void Main(string[] args)
        {
            var L = Convert.ToInt32(Console.ReadLine());
            var A = new List<string>();
            var str = Console.ReadLine();

            while (str != null)
            {
                A.Add(str);
                str = Console.ReadLine();
            }
            var count = 0;

            for (int i = 0; i < A.Count(); i++)
            {
                count++;
                if (A[i].Length > L)
                    break;
            }

            var res = A
                .Take(count)
                .Where(l => char.IsLetter(l[l.Length - 1]))
                .OrderByDescending(x => x)
                .ThenBy(l => l)
                .ToList();

            foreach (var element in res)
            {
                Console.WriteLine(element);
            }
        }

    }
}
