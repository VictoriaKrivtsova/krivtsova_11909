﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace flappyBirds
{
    public partial class FlappyBird : Form
    {
        int gravity = 15;
        int pipeSpeed = 10;
        int score = 0;
        int check = 5;
        public FlappyBird()
        {
            InitializeComponent();
        }

        private void gameTimeEvent(object sender, EventArgs e)
        {
            flapBird.Top += gravity;
            pipeBottom.Left -= pipeSpeed;
            pipeTop.Left -= pipeSpeed;
            scoreText.Text = "Score: " + score;


            if (pipeBottom.Left < -150)
            {
                score++;
                pipeBottom.Left = 800;
            }

            if (pipeTop.Left < -180)
            {
                score++;
                pipeTop.Left = 950;
            }


            if (flapBird.Bounds.IntersectsWith(pipeBottom.Bounds) || flapBird.Bounds.IntersectsWith(pipeTop.Bounds) ||
                                                                        flapBird.Bounds.IntersectsWith(ground.Bounds))
            {
                GameEnd();
            }

            if (score > check)
            {
                pipeSpeed += 5;
                check += 5;
            }

            if (flapBird.Top < -25)
            {
                GameEnd();
            }

        }

        private void gameKeyIsDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                gravity = -15;
            }
        }

        private void gameKeyIsUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                gravity = 15;
            }
        }

        private void GameEnd()
        {
            gameTimer.Stop();
            scoreText.Text += "   Game over!";
        }
    }
}
