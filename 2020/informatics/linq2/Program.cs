﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linq2
{
    class Program
    {
        static void Main(string[] args)
        {
            var K = Convert.ToInt32(Console.ReadLine());
            var list = Console.ReadLine().Split(' ').Select(Int32.Parse).ToList();

            var res1 = list.Where(x => (x % 2 == 0));
            var res2 = list.Skip(K - 1);

            var res = res1.Except(res2);

            Console.WriteLine(res);
        }
    }
}
