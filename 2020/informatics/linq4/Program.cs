﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linq4
{
    class Program
    {
        static void Main(string[] args)
        {
            var A = Console.ReadLine().Split(' ').Select(Int32.Parse).ToList();

            var res = A
                .Where(x => x % 2 != 0)
                .Select(x => x.ToString())
                .OrderBy(x => x)
                .ToList();

            Console.WriteLine(res);
        }
    }
}
