﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poker
{
    class DrawCards
    {
        //draw cards outlines
        public static void DrawCardOutline(int xcoor, int ycoor)
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;

            int x = xcoor * 12;
            int y = ycoor;

            Console.SetCursorPosition(x, y);
            Console.Write(" __________\n");//верхняя часть карты

            for (int i = 0; i < 10; i++)
            {
                Console.SetCursorPosition(x, y + 1 + i);

                if (i != 9)
                    Console.WriteLine("|          |");//левая и правая границы карты
                else
                    Console.WriteLine("|__________|");//нижняя границы карты
            }
        }

        //отображение масти и значения карты внутри ее контура
        public static void DrawCardSuitValue(Card card, int xcoor, int ycoor)
        {
            char cardSuit = ' ';
            int x = xcoor * 12;
            int y = ycoor;

            //кодирует каждый массив байтов из CodePage437 в символ
            //черви и бубны красные, крести и пики черные
            

            switch (card.MySuit)
            {
                case Card.Suit.Hearts:
                    cardSuit = Encoding.GetEncoding(437).GetChars(new byte[] { 3 })[0];
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case Card.Suit.Diamonds:
                    cardSuit = Encoding.GetEncoding(437).GetChars(new byte[] { 4 })[0];
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case Card.Suit.Clubs:
                    cardSuit = Encoding.GetEncoding(437).GetChars(new byte[] { 5 })[0];
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
                case Card.Suit.Spades:
                    cardSuit = Encoding.GetEncoding(437).GetChars(new byte[] { 6 })[0];
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
            }

            //отобразить сомвол и значение на карте
            Console.SetCursorPosition(x + 5, y + 4);
            Console.Write(cardSuit);
            Console.SetCursorPosition(x + 4, y + 6);
            Console.Write(card.MyValue);        
        }
    }
}
