﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Linq;

namespace Poker
{
    class DealCards : DeckOfCards
    {
        private Card[] playerHand;
        private Card[] computerHand;
        private Card[] sortedPlayerHand;
        private Card[] sortedComputerHand;

        public DealCards()
        {
            playerHand = new Card[5];
            sortedPlayerHand = new Card[5];
            computerHand = new Card[5];
            sortedComputerHand = new Card[5];
        }

        public void Deal()
        {
            SetUpDeck();//создает набор карт и тасует их;
            GetHand();
            SortCards();
            displayCards();
            evaluateHands();
        }

        public void GetHand()
        {
            //5 карт для игрока
            for (int i = 0; i < 5; i++)
                playerHand[i] = GetDeck[i];
            //5 карт для компьютера
            for (int i = 5; i < 10; i++)
            {
                computerHand[i - 5] = GetDeck[i];
            }

        }

        public void SortCards()
        {
            var queryPlayer = from hand in playerHand
                              orderby hand.MyValue
                              select hand;

            var queryComputer = from hand in computerHand
                                orderby hand.MyValue
                                select hand;

            var index = 0;
            foreach (var item in queryPlayer.ToList())
            {
                sortedPlayerHand[index] = item;
                index++;
            }

            index = 0;
            foreach (var item in queryComputer.ToList())
            {
                sortedComputerHand[index] = item;
                index++;
            }
        }

        public void displayCards()
        {
            Console.Clear();
            int x = 0;//x позиция курсора. Будем двигать его влево и вправо
            int y = 1;//y позиция курсора. Будем двигать вверх и вниз

            //выводим карты игрока
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Playr's Hand: ");
            for (int i = 0; i < 5; i++)
            {
                DrawCards.DrawCardOutline(x, y);
                DrawCards.DrawCardSuitValue(sortedPlayerHand[i], x, y);
                x++;//двигаем вправо
            }
            y = 15;//карты компьютера должны находиться ниже карт игрока
            x = 0;//сбрасываем позицию по х
            Console.SetCursorPosition(x, 14);
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Computer's Hand: ");
            for (int i = 5; i < 10; i++)
            {
                DrawCards.DrawCardOutline(x, y);
                DrawCards.DrawCardSuitValue(sortedComputerHand[i - 5], x, y);
                x++;//двигаем вправо
            }
        }

        public void evaluateHands()
        {
            HandEvaluator playerHandEvaluator = new HandEvaluator(sortedPlayerHand);
            HandEvaluator computerHandEvaluator = new HandEvaluator(sortedComputerHand);

            Hand playerHand = playerHandEvaluator.EvaluateHand();
            Hand computerHand = computerHandEvaluator.EvaluateHand();

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\n\n\n\n\n\nPlayer's Hand: " + playerHand);
            Console.WriteLine("\nComputer's Hand: " + computerHand);

            if (playerHand > computerHand)
            {
                Console.WriteLine("Player's Wins!");
            }
            else if (playerHand < computerHand)
            {
                Console.WriteLine("Computer's Wins!");
            }
            else
            {
                if (playerHandEvaluator.HandValues.Total > computerHandEvaluator.HandValues.Total)
                    Console.WriteLine("Player's Win's!");
                else if (playerHandEvaluator.HandValues.Total < computerHandEvaluator.HandValues.Total)
                    Console.WriteLine("Computer's Wins!");
                else if (playerHandEvaluator.HandValues.HighCard > computerHandEvaluator.HandValues.HighCard)
                    Console.WriteLine("Player's Wins!");
                else if (playerHandEvaluator.HandValues.HighCard < computerHandEvaluator.HandValues.HighCard)
                    Console.WriteLine("Computer's Wins!");
                else
                    Console.WriteLine("No one Wins...");
            }


        }
    }
}
