﻿using System;
using System.Text; 
namespace Poker
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            Console.SetWindowSize(65, 40);
            //remove scroll bars by setting the butter to the actual window sixe
            Console.BufferWidth = 65;
            Console.BufferHeight = 40;
            
            Console.Title = "Poker Game";
            DealCards dc = new DealCards();
            bool quit = false;

            while (!quit)
            {
                dc.Deal();
                char selection = ' ';
                while (!selection.Equals('Y') && !selection.Equals('N'))
                {
                    Console.WriteLine("Play again? Y-N");
                    selection = Convert.ToChar(Console.ReadLine().ToUpper());

                    if (selection.Equals('Y'))
                        quit = false;
                    else if (selection.Equals('N'))
                        quit = true;
                    else
                        Console.WriteLine("Error. Try again");
                }
            }
            Console.ReadKey();
        }
    }
}
