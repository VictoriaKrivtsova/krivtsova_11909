﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poker
{
    class DeckOfCards : Card
    {
        const int Num_Of_Cards = 52;//число карт
        private Card[] deck;//массив всех карт

        public DeckOfCards()
        {
            deck = new Card[Num_Of_Cards];
        }

        public Card[] GetDeck { get { return deck; } }//текущая колода

        //создаем доску из 52 карт: 13 значений по 4 масти каждое
        public void SetUpDeck()
        {
            int i = 0;
            foreach (Suit s in Enum.GetValues(typeof(Suit)))
            {
                foreach (Value v in Enum.GetValues(typeof(Value)))
                {
                    deck[i] = new Card { MySuit = s, MyValue = v };
                    i++;
                }
            }
            ShuffleCards();
        }

        //Перетасовка карт
        public void ShuffleCards()
        {
            Random rand = new Random();
            Card temp;

            //перетасуем карты 1000 раз
            for (int shuffleTimes = 0; shuffleTimes < 1000; shuffleTimes++)
            {
                for (int i = 0; i < Num_Of_Cards; i++)
                {
                    //меняем карты(swap cards)
                    int secondCardIndex = rand.Next(13);
                    temp = deck[i];
                    deck[i] = deck[secondCardIndex];
                    deck[secondCardIndex] = temp;
                }
            }
        }
    }
}
