﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poker
{
    class Card
    {
        public enum Suit
        {
            Hearts,
            Diamonds,
            Spades,
            Clubs
        }

        public enum Value
        {
            Two = 2, Three, Four, Five, Six, Seven, 
            Eight, Nine, Ten, Jack, Queen, King, Ace
        }

        public Suit MySuit { get; set; }
        public Value MyValue { get; set; }
    }
}
