﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace regex
{       
    class Program
    {
        static void Task04b()
            {
                Random rnd = new Random();
                var regex = @"^[02468]{3,6}$";

                var counter = 0;
                var all_number = 0;
                List<int> list = new List<int>();

                while (counter != 10)
                {
                    var number = rnd.Next();
                    if (Regex.IsMatch(number.ToString(), regex))
                    {
                        counter++;
                        list.Add(number);
                    }

                    all_number++;
                }
                Console.WriteLine("Общее количество: " + all_number);
                foreach (var elem in list)
                {
                    Console.WriteLine(elem);
                }
            }
        
        static void Task01a()
            {
                string a = @"(\+|\-|)(\d+)(\.\d+|)(\(\d+\)|)";

                while (true)
                {
                    var input = Console.ReadLine();
                    var result = Regex.Match(input, a).Value;


                    if (result == "-0" || result != input || (result.Contains(".") && result[result.Length - 1] == '0' || (result.Length != 1 && result[0] == '0' && result[1] != '.')))
                    {
                        Console.WriteLine("No!");
                    }
                    else
                    {
                        Console.WriteLine(result);
                    }
                }
            }
        static void Task01b()
            {
                string a = @"(\d{1,2}/\d{1,2}/\d{4}\s\d{1,2}\:\d{1,2})";

                while (true)
                {

                    var input = Console.ReadLine();
                    var check = Regex.Match(input, a);
                    var split = check.Value.Split('/', ' ', ':');
                    if (Convert.ToInt32(split[2]) <= 1978 && Convert.ToInt32(split[2]) >= 1237)
                    {
                        if (Convert.ToInt32(split[2]) == 1237)
                        {
                            if (Convert.ToInt32(split[1]) >= 3 && Convert.ToInt32(split[0]) >= 6 && Convert.ToInt32(split[3]) >= 12)
                                Console.WriteLine(check);
                            else return;
                            return;
                        }
                        else if (Convert.ToInt32(split[1]) <= 27 && Convert.ToInt32(split[2]) == 1978 && Convert.ToInt32(split[3]) <= 21 && Convert.ToInt32(split[0]) <= 2)
                        {
                            if (Convert.ToInt32(split[3]) == 21)
                            {
                                if (Convert.ToInt32(split[4]) <= 35)
                                    Console.WriteLine(check);
                                else return;
                            }
                            else Console.WriteLine(check);
                            return;
                        }
                        else
                        {
                            Console.WriteLine(check);
                            return;
                        }
                    }
                }
            }
        
        static void Task03c()
            {
                Random rnd = new Random();

                string regex = @"^\d*[02468]$";

                var counter = 0;
                var all_number = 0;
                List<int> list = new List<int>();

                while (counter != 10)
                {
                    var number = rnd.Next();
                    if (Regex.IsMatch(number.ToString(), regex))
                    {
                        counter++;
                        list.Add(number);
                    }

                    all_number++;
                }
                Console.WriteLine("Общее количество: " + all_number);
                foreach (var elem in list)
                {
                    Console.WriteLine(elem);
                }

            }
        
        static void Task02c()
            {
                string a = @"((01)+(0|))|((10)+(1|))|0+|1+";

                Console.Write("Number of strings: ");
                var count = Convert.ToInt32(Console.ReadLine());

                List<int> results = new List<int>();
                for (int i = 0; i < count; i++)
                {
                    var input = Console.ReadLine();
                    var res = Regex.Match(input, a);

                    if (res.Length == input.Length)
                        results.Add(i + 1);
                }

                foreach (var elem in results)
                {
                    Console.WriteLine(elem);
                }
            }
        static void Task03b()
            {
                Random rnd = new Random();

                string regex = @"^[13579]*[02468]{0,2}([13579]+[02468]{0,2})*$";

                var counter = 0;
                var all_number = 0;
                List<int> list = new List<int>();

                while (counter != 10)
                {
                    var number = rnd.Next();
                    if (Regex.IsMatch(number.ToString(), regex))
                    {
                        counter++;
                        list.Add(number);
                    }

                    all_number++;
                }
                Console.WriteLine("Общее количество: " + all_number);
                foreach (var elem in list)
                {
                    Console.WriteLine(elem);
                }
            }
        
        static void Task05b()
            {
                Random rnd = new Random();
                var regex = @"^\d*([02468]{2}\d*){2,}$";

                var counter = 0;
                var all_number = 0;
                List<int> list = new List<int>();

                while (counter != 10)
                {
                    var number = rnd.Next();
                    if (Regex.IsMatch(number.ToString(), regex))
                    {
                        counter++;
                        list.Add(number);
                    }

                    all_number++;
                }
                Console.WriteLine("Общее количество: " + all_number);
                foreach (var elem in list)
                {
                    Console.WriteLine(elem);
                }
            }
    }
}

