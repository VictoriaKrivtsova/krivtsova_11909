﻿using iTextSharp.tool.xml.html;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace selfHostedHttp
{
	class Program
	{
		static void Main(string[] args)
		{

			HttpListener listener = new HttpListener();
			listener.Prefixes.Add("http://localhost:8080/");
			listener.Start();

            while (true)
            {
				Console.WriteLine("Сервер начал прослушивание порта 8080");
				HttpListenerContext context = listener.GetContext();
				HttpListenerRequest request = context.Request;
				HttpListenerResponse response = context.Response;
				string content = "";

				var url = request.Url.LocalPath.ToString();

				Console.WriteLine(request.Url.LocalPath.ToString());

				if (url == "/head/")
					content = head.Value;
				else if (url == "/cv/")
					content = cv.Value;
				else if (url == "/photos/")
					content = photos.Value;
				else if (url == "/contact/")
					content = contact.Value;
				else if (url == "/biography/")
					content = biography.Value;



				byte[] bytes = Encoding.UTF8.GetBytes(content);

				response.ContentLength64 = bytes.Length;

				Stream sw = response.OutputStream;
				sw.Write(bytes, 0, bytes.Length);
				sw.Close();
			}
		}
	}
}
