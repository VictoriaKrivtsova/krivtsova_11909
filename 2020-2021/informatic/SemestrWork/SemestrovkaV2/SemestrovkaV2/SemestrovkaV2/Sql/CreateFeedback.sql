﻿Create table dbo.Feedback(
	Id int primary key identity(1,1),
	AuthorName nvarchar(100) NOT NULL,
	AuthorId int NOT NULL,
	SneakerId int NOT NULL,
	FeedbackText nvarchar(1000) NOT NULL,
);