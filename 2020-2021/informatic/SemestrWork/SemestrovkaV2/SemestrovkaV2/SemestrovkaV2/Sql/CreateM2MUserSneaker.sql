﻿create table dbo.AppUser_Sneaker(
	AppUserId int not null,
	SneakerId int not null,
	Status varchar(100),
	constraint PK_APPUSER_SNEAKER primary key clustered ( AppUserId, SneakerId )
);