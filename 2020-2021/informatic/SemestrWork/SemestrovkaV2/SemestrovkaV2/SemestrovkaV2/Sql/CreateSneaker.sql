﻿Create table dbo.Sneaker(
	Id int primary key identity(1,1),
	Name nvarchar(100) NOT NULL,
	Description nvarchar(1000) NOT NULL,
	ImageName nvarchar(100),
	Price numeric(10,5)
);