#pragma checksum "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\Account\Profile.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2c2df341a4cd9f088401cb9fc4118a34e7dceda4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Account_Profile), @"mvc.1.0.view", @"/Views/Account/Profile.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Account/Profile.cshtml", typeof(AspNetCore.Views_Account_Profile))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\_ViewImports.cshtml"
using SemestrovkaV2;

#line default
#line hidden
#line 2 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\_ViewImports.cshtml"
using SemestrovkaV2.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2c2df341a4cd9f088401cb9fc4118a34e7dceda4", @"/Views/Account/Profile.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"37f4688b6bff7bfcce5efb0ddf44da434ad35994", @"/Views/_ViewImports.cshtml")]
    public class Views_Account_Profile : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<SemestrovkaV2.Models.ProfileModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(42, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\Account\Profile.cshtml"
  
    ViewData["Title"] = "Profile";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
            BeginContext(134, 99, true);
            WriteLiteral("\r\n<h2>Profile</h2>\r\n\r\n<div>\r\n    <hr />\r\n    <dl class=\"dl-horizontal\">\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(234, 38, false);
#line 14 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\Account\Profile.cshtml"
       Write(Html.DisplayNameFor(model => model.Id));

#line default
#line hidden
            EndContext();
            BeginContext(272, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(316, 34, false);
#line 17 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\Account\Profile.cshtml"
       Write(Html.DisplayFor(model => model.Id));

#line default
#line hidden
            EndContext();
            BeginContext(350, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(394, 41, false);
#line 20 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\Account\Profile.cshtml"
       Write(Html.DisplayNameFor(model => model.Email));

#line default
#line hidden
            EndContext();
            BeginContext(435, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(479, 37, false);
#line 23 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\Account\Profile.cshtml"
       Write(Html.DisplayFor(model => model.Email));

#line default
#line hidden
            EndContext();
            BeginContext(516, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(560, 45, false);
#line 26 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\Account\Profile.cshtml"
       Write(Html.DisplayNameFor(model => model.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(605, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(649, 41, false);
#line 29 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\Account\Profile.cshtml"
       Write(Html.DisplayFor(model => model.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(690, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(734, 44, false);
#line 32 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\Account\Profile.cshtml"
       Write(Html.DisplayNameFor(model => model.LastName));

#line default
#line hidden
            EndContext();
            BeginContext(778, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(822, 40, false);
#line 35 "C:\Users\victo\OneDrive\Desktop\SemestrovkaV2\SemestrovkaV2\SemestrovkaV2\Views\Account\Profile.cshtml"
       Write(Html.DisplayFor(model => model.LastName));

#line default
#line hidden
            EndContext();
            BeginContext(862, 34, true);
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<SemestrovkaV2.Models.ProfileModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
