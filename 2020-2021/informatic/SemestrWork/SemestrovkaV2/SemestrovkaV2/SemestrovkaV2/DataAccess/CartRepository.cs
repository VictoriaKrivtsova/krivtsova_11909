﻿using SemestrovkaV2.DataAccess.Abstract;
using SemestrovkaV2.DbModels;
using SemestrovkaV2.Helpers;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SemestrovkaV2.DataAccess
{
    public class CartRepository : ICartRepository
    {
        private readonly string connectionString;

        public CartRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Add(Cart obj)
        {
            string commandText = "insert into dbo.AppUser_Sneaker (AppUserId, SneakerId, Status) values ("+ obj.UserId +","+ obj.SneakerId +",'in_cart');";

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(commandText, connection))
                {
                    var reader = command.ExecuteReader();
                }
            }
        }

        public void Delete(Cart obj)
        {
            throw new System.NotImplementedException();
        }

        public Cart FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Cart> FindAllByUserId(int id)
        {
            string commandText = "Select * From dbo.AppUser_Sneaker where dbo.AppUser_Sneaker.AppUserId = " + id.ToString();

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(commandText, connection))
                {
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        yield return SqlHelper.GetFromReader<Cart>(reader);
                    }
                }
            }
        }

        public IEnumerable<Cart> List()
        {
            string commandText = "Select * From dbo.AppUser_Sneaker";

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(commandText, connection))
                {
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        yield return SqlHelper.GetFromReader<Cart>(reader);
                    }
                }
            }
        }

        public void Update(Cart obj)
        {
            throw new System.NotImplementedException();
        }
    }
}