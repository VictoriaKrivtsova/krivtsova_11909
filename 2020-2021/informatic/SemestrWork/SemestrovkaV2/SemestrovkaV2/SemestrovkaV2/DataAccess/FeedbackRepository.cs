﻿using SemestrovkaV2.DataAccess.Abstract;
using SemestrovkaV2.DbModels;
using SemestrovkaV2.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SemestrovkaV2.DataAccess
{
    public class FeedbackRepository : IFeedbackRepository
    {
        private readonly string connectionString;

        public FeedbackRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Add(Feedback obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(Feedback obj)
        {
            throw new NotImplementedException();
        }

        public Feedback FindById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Feedback> List()
        {
            throw new NotImplementedException();
        }

        public void Update(Feedback obj)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Feedback> AllBySneakerId(int sneakerId)
        {
            string commandText = "SELECT * FROM dbo.Feedback WHERE SneakerId = '" + sneakerId +"';";

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(commandText, connection))
                {
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        yield return SqlHelper.GetFromReader<Feedback>(reader);
                    }
                }
            }
        }

        public void Save (Feedback feedback)
        {
            string commandText = "INSERT INTO dbo.Feedback(SneakerId, AuthorId, AuthorName, FeedbackText) " +
                "VALUES (@SneakerId, @AuthorId, @AuthorName, @Text);";
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(commandText, connection))
                {
                    command.Parameters.AddWithValue("@SneakerId", feedback.SneakerId);
                    command.Parameters.AddWithValue("@AuthorId", feedback.AuthorId);
                    command.Parameters.AddWithValue("@AuthorName", feedback.AuthorName);
                    command.Parameters.AddWithValue("@Text", feedback.FeedbackText);

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
