﻿using SemestrovkaV2.DbModels;

namespace SemestrovkaV2.DataAccess.Abstract
{
    public interface IUserRepository : IRepository<User>
    {
        User FindByEmail(string email);
    }
}
