﻿using SemestrovkaV2.DbModels;

namespace SemestrovkaV2.DataAccess.Abstract
{
    interface ICartRepository: IRepository<Cart>
    {
    }
}