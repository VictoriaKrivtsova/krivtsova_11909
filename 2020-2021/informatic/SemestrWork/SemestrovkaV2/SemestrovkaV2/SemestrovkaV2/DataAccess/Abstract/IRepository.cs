﻿using System.Collections.Generic;

namespace SemestrovkaV2.DataAccess.Abstract
{
    public interface IRepository<T>
    {
        T FindById(int id);
        void Add(T obj);
        void Delete(T obj);
        void Update(T obj);
        IEnumerable<T> List();
    }
}
