﻿using SemestrovkaV2.DbModels;

namespace SemestrovkaV2.DataAccess.Abstract
{
    public interface ISneakerRepository : IRepository<Sneaker>
    {
    }
}
