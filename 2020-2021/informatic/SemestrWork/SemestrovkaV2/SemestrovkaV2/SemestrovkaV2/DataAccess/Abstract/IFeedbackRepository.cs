﻿using SemestrovkaV2.DbModels;
using System.Collections.Generic;

namespace SemestrovkaV2.DataAccess.Abstract
{
    public interface IFeedbackRepository: IRepository<Feedback>
    {
        IEnumerable<Feedback> AllBySneakerId(int sneakerId);
        void Save(Feedback feedback);
    }
}
