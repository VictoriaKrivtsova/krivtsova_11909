﻿using SemestrovkaV2.DataAccess.Abstract;
using SemestrovkaV2.DbModels;
using SemestrovkaV2.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;

namespace SemestrovkaV2.DataAccess
{
    public class UserRepository : IUserRepository
    {
        private string connectionString;

        public UserRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Add(User user)
        {
            string commandText = "insert into dbo.AppUser(Email, Password, FirstName, LastName)" +
                "VAlUES(@Email, @Password, @FirstName, @LastName)";

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(commandText, connection))
                {
                    command.Parameters.AddWithValue("@Id", user.Id);
                    command.Parameters.AddWithValue("@Email", user.Email);
                    command.Parameters.AddWithValue("@Password", user.Password);
                    command.Parameters.AddWithValue("@FirstName", user.FirstName);
                    command.Parameters.AddWithValue("@LastName", user.LastName);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void Delete(User obj)
        {
            throw new System.NotImplementedException();
        }

        public User FindByEmail(string email)
        {
            string commandText = "Select * From dbo.AppUser where Email = @email";

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(commandText, connection))
                {
                    command.Parameters.AddWithValue("@email", email);

                    var reader = command.ExecuteReader();

                    bool hasUser = reader.Read();

                    if (!hasUser)
                    {
                        return null;
                    }

                    return SqlHelper.GetFromReader<User>(reader);
                }
            }
        }

        public User FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<User> List()
        {
            throw new NotImplementedException();
        }

        public void Update(User obj)
        {
            throw new System.NotImplementedException();
        }
    }
}
