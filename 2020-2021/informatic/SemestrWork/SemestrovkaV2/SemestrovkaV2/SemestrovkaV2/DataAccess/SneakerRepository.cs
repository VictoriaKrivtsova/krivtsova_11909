﻿using SemestrovkaV2.DataAccess.Abstract;
using SemestrovkaV2.DbModels;
using SemestrovkaV2.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SemestrovkaV2.DataAccess
{
    public class SneakerRepository : ISneakerRepository
    {
        private readonly string connectionString;

        public SneakerRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Add(Sneaker obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(Sneaker obj)
        {
            throw new NotImplementedException();
        }

        public Sneaker FindById(int id)
        {
            string commandText = "SELECT * FROM dbo.Sneaker WHERE Id = " + id;

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(commandText, connection))
                {
                    var reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        return SqlHelper.GetFromReader<Sneaker>(reader);
                    }
                }
                return null;
            }

        }

        public IEnumerable<Sneaker> List()
        {
            string commandText = "Select * From dbo.Sneaker";

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(commandText, connection))
                {
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        yield return SqlHelper.GetFromReader<Sneaker>(reader);
                    }
                }
            }
        }

        public void Update(Sneaker obj)
        {
            throw new NotImplementedException();
        }
    }
}
