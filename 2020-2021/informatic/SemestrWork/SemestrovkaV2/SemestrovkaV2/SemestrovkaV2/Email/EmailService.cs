﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SemestrovkaV2.Email
{
    public class EmailService
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(MailboxAddress.Parse("krm.shrftdnv@gmail.com"));
            emailMessage.To.Add(MailboxAddress.Parse(email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message,
            };
            emailMessage.Sender = new MailboxAddress("krm.shrftdnv@gmail.com");

            using (var client = new SmtpClient("smtp.gmail.com", 587))
            {
                client.Credentials = new System.Net.NetworkCredential("krm.shrftdnv@gmail.com", "KALENDARNO00");
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                using (MailMessage mailMessage = new MailMessage())
                {
                    Encoding encoding = System.Text.Encoding.UTF8;
                    mailMessage.IsBodyHtml = false;
                    mailMessage.SubjectEncoding = encoding;
                    mailMessage.BodyEncoding = encoding;
                    mailMessage.From = new MailAddress("krm.shrftdnv@gmail.com", "Karim", encoding);
                    mailMessage.Bcc.Add(new MailAddress(email, "Customer", encoding));
                    mailMessage.Subject = subject;
                    mailMessage.Body = message;

                    client.Send(mailMessage);
                }
            }
        }

        public static bool SendMail(string FromName,
            string FromAddr, string ToName, string ToAddr, string Subject)
        {
           
            return false;
        }

        //MailKit.Net.Smtp.SmtpClient
        public void SendEmailCustom(string email, string subject, string messageText)
        {
            MimeMessage message = new MimeMessage();
            message.From.Add(new MailboxAddress("noreply", "alphadoggy@yandex.ru")); //отправитель сообщения
            message.To.Add(new MailboxAddress("", email)); //адресат сообщения
            message.Subject = "Сообщение от MailKit"; //тема сообщения
            message.Body = new BodyBuilder() { HtmlBody = "<div style=\"color: green;\">Сообщение от MailKit</div>" }.ToMessageBody(); //тело сообщения (так же в формате HTML)

            using (MailKit.Net.Smtp.SmtpClient client = new MailKit.Net.Smtp.SmtpClient())
            {
                client.Connect("smtp.gmail.com", 587, true); //либо использум порт 465
                client.Authenticate("krm.shrftdnv@gmail.com", "KALENDARNO00"); //логин-пароль от аккаунта
                client.Send(message);

                client.Disconnect(true);
            }

        }
    }
}
