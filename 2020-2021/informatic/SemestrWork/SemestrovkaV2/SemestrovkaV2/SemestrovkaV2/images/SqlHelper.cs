﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SemestrovkaV2.Helpers
{
    public static class SqlHelper
    {
        public static T GetFromReader<T>(SqlDataReader reader)
            where T: new()
        {
            T result = new T();

            for (int i = 0; i < reader.FieldCount; i++)
            {
                PropertyInfo propertyInfo = result.GetType().GetProperty(reader.GetName(i));
                var value = reader.GetValue(i);

                propertyInfo.SetValue(result, Convert.ChangeType(value, propertyInfo.PropertyType), null);
            }

            return result;
        }
    }
}
