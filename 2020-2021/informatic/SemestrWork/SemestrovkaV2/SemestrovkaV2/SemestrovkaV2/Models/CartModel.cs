﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SemestrovkaV2.Models
{
    public class CartModel
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public void AddItem(SneakerDetailInfo sneaker)
        {
            CartLine line = lineCollection
                .Where(s => s.Sneaker.Id == sneaker.Id)
                .FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new CartLine { Sneaker = sneaker, Quantity = 1 });
            }
            else
            {
                line.Quantity += 1;
            }
        }

        public void Remove(SneakerDetailInfo sneaker, int quantity)
        {
            CartLine line = lineCollection
                .Where(s => s.Sneaker.Id == sneaker.Id)
                .FirstOrDefault();

            line.Quantity = (line.Quantity - quantity > 0) ? line.Quantity - quantity : 0;
            if (line.Quantity <= 0)
            {
                lineCollection.Remove(line);
            }
        }

        public void RemoveLine(SneakerDetailInfo sneaker)
        {
            lineCollection.RemoveAll(l => l.Sneaker.Id == sneaker.Id);
        }

        public decimal ComputeTotalPrice()
        {
            return lineCollection.Sum(l => l.Sneaker.Price * l.Quantity);
        }

        public void Clear()
        {
            lineCollection.Clear();
        }

        public IEnumerable<CartLine> Lines { get { return lineCollection; } }

    }

    public class CartLine
    {
        public SneakerDetailInfo Sneaker { get; set; }
        public int Quantity { get; set; }
    }
}