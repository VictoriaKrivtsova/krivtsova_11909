﻿using SemestrovkaV2.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemestrovkaV2.Models
{
    public class FeedbackModel
    {
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }

        public string Text { get; set; }
        public int SneakerId { get; set; }

        public static FeedbackModel FeedbackModelFromDBModel(Feedback feedbackFromDB)
        {
            return new FeedbackModel
            {
                AuthorId = feedbackFromDB.AuthorId,
                AuthorName = feedbackFromDB.AuthorName,
                SneakerId = feedbackFromDB.SneakerId,
                Text = feedbackFromDB.FeedbackText,
            };
        }
        public static List<FeedbackModel> GetFeedbackModelsFromDBModels(IEnumerable<Feedback> feedbackModelsFromDB)
        {
            List<FeedbackModel> feedbackModels = new List<FeedbackModel>();
            foreach(var feedBackFromDB in feedbackModelsFromDB)
            {
                feedbackModels.Add(FeedbackModelFromDBModel(feedBackFromDB));
            }
            return feedbackModels;
        }
    }
}
