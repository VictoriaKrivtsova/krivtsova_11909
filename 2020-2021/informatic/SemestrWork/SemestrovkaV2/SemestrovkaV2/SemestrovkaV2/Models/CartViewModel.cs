﻿using System.Collections.Generic;

namespace SemestrovkaV2.Models
{
    public class CartViewModel
    {
        public CartModel Cart { get; set; }
        public List<CartLine> Sneakers { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
