﻿namespace SemestrovkaV2.Models
{
    public class SneakerListViewModel
    {
        public SneakerDetailInfo[] Sneakers { get; set; }

        public int TotalCount { get; set; }
    }
}
