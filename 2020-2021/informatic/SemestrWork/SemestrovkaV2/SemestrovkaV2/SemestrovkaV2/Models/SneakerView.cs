﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemestrovkaV2.Models
{
    public class SneakerView
    {
        public SneakerDetailInfo Sneaker { get; set; }
        public List<FeedbackModel> Feedbacks { get; set; }
    }
}
