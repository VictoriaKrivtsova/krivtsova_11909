﻿using SemestrovkaV2.DbModels;

namespace SemestrovkaV2.Models
{
    public class SneakerDetailInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public string ImageSource { get; set; }

        public static SneakerDetailInfo SneakerFromDbModel(Sneaker s)
        {
            return new SneakerDetailInfo
            {
                Description = s.Description,
                Id = s.Id,
                Name = s.Name,
                Price = s.Price,
                ImageSource = "images/" + s.ImageName
            };
        }
    }
}
