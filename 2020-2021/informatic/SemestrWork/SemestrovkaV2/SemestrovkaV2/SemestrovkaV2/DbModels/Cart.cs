﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemestrovkaV2.DbModels
{
    public class Cart
    {
        public int UserId { get; set; }

        public int SneakerId { get; set; }
        
        public string Status { get; set; }

    }
}
