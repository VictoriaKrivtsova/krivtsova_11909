﻿namespace SemestrovkaV2.DbModels
{
    public class Feedback
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }
        public string FeedbackText { get; set; }
        public int SneakerId { get; set; }
    }
}
