﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SemestrovkaV2.DataAccess.Abstract;
using SemestrovkaV2.DbModels;
using SemestrovkaV2.Models;

namespace SemestrovkaV2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISneakerRepository sneakerRepository;

        public HomeController(ISneakerRepository sneakerRepository)
        {
            this.sneakerRepository = sneakerRepository;
        }

        public IActionResult Index()
        {
            var allSneakers = this.sneakerRepository.List();

            return View(new SneakerListViewModel
            {
                Sneakers = allSneakers.Select(x => new SneakerDetailInfo
                {
                    Description = x.Description,
                    Id = x.Id,
                    Name = x.Name,
                    Price = x.Price,
                    ImageSource = "images/" + x.ImageName,
                }).ToArray(),
                TotalCount = allSneakers.Count(),
            });
        }

        
    }
}
