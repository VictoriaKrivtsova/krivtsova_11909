﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SemestrovkaV2.DataAccess.Abstract;
using SemestrovkaV2.DbModels;
using SemestrovkaV2.Email;
using SemestrovkaV2.Models;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemestrovkaV2.Controllers
{
    public class CartController : Controller
    {
        private readonly ISneakerRepository sneakerRepository;

        public CartController(ISneakerRepository sneakerRepository)
        {
            this.sneakerRepository = sneakerRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var cart = GetCart();

            return View(new CartViewModel
            {
                Cart = cart,
                Sneakers = cart.Lines.ToList(),
                TotalPrice = cart.ComputeTotalPrice()
            });
        }

        [HttpPost]
        public IActionResult AddToCart(int id)
        {
            Sneaker s = sneakerRepository.FindById(id);
            if (s != null)
            {
                var sneaker = SneakerDetailInfo.SneakerFromDbModel(s);
                var cart = GetCart();
                cart.AddItem(sneaker);
                UpdateCart(cart);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult RemoveFromCart(int id)
        {
            Sneaker s = sneakerRepository.FindById(id);
            if (s != null)
            {
                var sneaker = SneakerDetailInfo.SneakerFromDbModel(s);
                var cart = GetCart();
                cart.RemoveLine(sneaker);
                UpdateCart(cart);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult RemoveOne(int id)
        {
            Sneaker s = sneakerRepository.FindById(id);
            if (s != null)
            {
                var sneaker = SneakerDetailInfo.SneakerFromDbModel(s);
                var cart = GetCart();
                cart.Remove(sneaker, 1);
                UpdateCart(cart);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrderAsync(string name, string email, string phoneNumber)
        {
            EmailService emailService = new EmailService();
            var customerEmailTheme = "Your order in SneakerShop";
            StringBuilder emailText = new StringBuilder();
            emailText.Append("You have ordered sneakers:\n");
            foreach (var line in GetCart().Lines)
            {
                emailText
                    .Append("- Name: ")
                    .Append(line.Sneaker.Name)
                    .Append("; Price: ")
                    .Append(line.Sneaker.Price)
                    .Append("; Quantity: ")
                    .Append(line.Quantity)
                    .Append("\n");
            }

            await emailService.SendEmailAsync(email, customerEmailTheme, emailText.ToString());

            var sellerEmailAddress = "victoria20011005@gmail.com";
            var sellerEmailTheme = "New order";
            emailText.Clear();
            emailText
                .Append("Customer ")
                .Append(name)
                .Append(" with email - ")
                .Append(email)
                .Append(" and phone number ")
                .Append(phoneNumber)
                .Append("have ordered these sneakers:\n");
            foreach (var line in GetCart().Lines)
            {
                emailText
                    .Append("- Name: ")
                    .Append(line.Sneaker.Name)
                    .Append("; Price: ")
                    .Append(line.Sneaker.Price)
                    .Append("; Quantity: ")
                    .Append(line.Quantity)
                    .Append("\n");
            }
            await emailService.SendEmailAsync(sellerEmailAddress, sellerEmailTheme, emailText.ToString());

            var cart = GetCart();
            cart.Clear();
            UpdateCart(cart);

            return RedirectToAction("Index");
        }


        private CartModel GetCart()
        {
            var serializedCart = HttpContext.Session.GetString("cart");

            if (serializedCart != null)
            {
                var deserializedCart = Newtonsoft.Json.JsonConvert.DeserializeObject<CartModel>(serializedCart);

                if (deserializedCart != null)
                {
                    return deserializedCart;
                }
            }
            var cart = new CartModel();
            HttpContext.Session.SetString(
                "cart",
                Newtonsoft.Json.JsonConvert.SerializeObject(cart));
            return cart;
        }

        private void UpdateCart(CartModel cart)
        {
            HttpContext.Session.SetString(
                "cart",
                Newtonsoft.Json.JsonConvert.SerializeObject(cart));
        }

    }
}
