﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SemestrovkaV2.DataAccess.Abstract;
using SemestrovkaV2.DbModels;
using SemestrovkaV2.Models;
using System.Collections.Generic;
using System.Linq;

namespace SemestrovkaV2.Controllers
{
    public class SneakerController : Controller
    {
        private readonly ISneakerRepository sneakerRepository;
        private readonly IUserRepository userRepository;
        private readonly IFeedbackRepository feedbackRepository;

        public SneakerController(ISneakerRepository sneakerRepository, IFeedbackRepository feedbackRepository, IUserRepository userRepository)
        {
            this.sneakerRepository = sneakerRepository;
            this.feedbackRepository = feedbackRepository;
            this.userRepository = userRepository;
        }

        [HttpGet]
        public IActionResult SneakerInfo(int id)
        {
            Sneaker s = sneakerRepository.FindById(id);
            if (s != null)
            {
                var sneaker = SneakerDetailInfo.SneakerFromDbModel(s);

                IEnumerable<Feedback> feedbacksFromDB = feedbackRepository.AllBySneakerId(id);

                return View(new SneakerView
                {
                    Sneaker = sneaker,
                    Feedbacks = FeedbackModel.GetFeedbackModelsFromDBModels(feedbacksFromDB).ToList(),
                });
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [Authorize]
        public IActionResult PostFeedback(string text, int id)
        {
            var emailClaim = User.Claims.First(x => x.Type == "Email");
            var emailValue = emailClaim.Value;
            var user = this.userRepository.FindByEmail(emailValue);

            Feedback feedback = new Feedback
            {
                AuthorId = user.Id,
                AuthorName = user.FirstName + ' ' + user.LastName,
                FeedbackText = text,
                SneakerId = id
            };

            feedbackRepository.Save(feedback);
            return RedirectToAction("SneakerInfo", new { id = id });
        }
    }
}
