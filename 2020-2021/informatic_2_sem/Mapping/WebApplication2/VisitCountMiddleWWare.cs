﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebApplication2
{
    public class VisitCountMiddleWWare
    {
        private readonly RequestDelegate _next;
        private double count = 0.5;
        public VisitCountMiddleWWare(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            count += 0.5;
            await context.Response.WriteAsync("Visit counts : " + count);
            await _next.Invoke(context);
        }
    }
}
