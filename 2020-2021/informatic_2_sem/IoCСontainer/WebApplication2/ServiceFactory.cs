﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace WebApplication2
{
    public class ServiceFactory
    {
        public static void getService()
        {
            var jsonString = System.IO.File.ReadAllText("config.json");
            var setti = JsonSerializer.Deserialize<Settings>(jsonString);
            switch (setti.service)
            {
                case "1":
                    IoCContainer.Register<ILoger, FileLoger>();
                    break;
                case "2":
                    IoCContainer.Register<ILoger, ConsoleLoger>();
                    break;
            }
        }
    }
}
