using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace WebApplication2
{
    public class Startup
    {

        private List<string> pass = new List<string>();
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(IoCContainer.Resolve<ILoger>() as ILoger);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseMiddleware<UserInformation>();
            app.UseMiddleware<CustomMiddleWare>();

            app.MapWhen(context =>
            {
                return context.Request.Query.ContainsKey("password") &&
                    context.Request.Query["password"] == "fog";
            }, Prifil);

            app.MapWhen(context =>
            {
                return context.Request.Query.ContainsKey("password") &&
                    context.Request.Query["password"] == "fogg";
            }, Group);

            app.MapWhen(context =>
            {
                return context.Request.Query.ContainsKey("password") &&
                    context.Request.Query["password"] == "info";
            }, Info);

            app.MapWhen(context =>
            {
                return context.Request.Query.ContainsKey("password") && 
                !pass.Contains(context.Request.Query["password"]);
            }, Error);

            app.UseMiddleware<VisitCountMiddleWWare>();



            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {

                    await context.Response.WriteAsync("");

                });
            });

        }

        private static void Info(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                string browser = "";
                string OperatioSystem = "";
                var a = System.IO.File.ReadAllText(@"wwwroot/page/info.html");

                foreach(var b in UserInformation.UserB)
                {
                    browser += String.Format("{0}: {1} \n", b.Key, b.Value);
                }

                foreach (var os in UserInformation.UserOS)
                {
                    OperatioSystem += String.Format("{0}: {1} \n", os.Key, os.Value);
                }

                var page = String.Format(a,OperatioSystem, browser);
                await context.Response.WriteAsync(page);
            });
        }

        private static void Prifil(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                var page = System.IO.File.ReadAllText("wwwroot/page/Profil.html");
                await context.Response.WriteAsync(page);
            });
        }

        private static void Error(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                var page = System.IO.File.ReadAllText("wwwroot/page/Error.html");
                await context.Response.WriteAsync(page);
            });
        }

        private static void Group(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                var page = System.IO.File.ReadAllText("wwwroot/page/Group.html");
                await context.Response.WriteAsync(page);
            });
        }
    }
}