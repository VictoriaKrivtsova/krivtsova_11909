﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebApplication2
{
    public class CustomMiddleWare
    {
        private readonly RequestDelegate _next;
        private string name;
        public CustomMiddleWare(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            name = context.Request.Query["name"];
            if (!String.IsNullOrEmpty(name))
            {
                await context.Response.WriteAsync("hello " + name + "\n");
            }
            else
            {
                var page = System.IO.File.ReadAllText("htmlpage.html");
                await context.Response.WriteAsync(page);
            }

            _next.Invoke(context);
        }
    }
}
