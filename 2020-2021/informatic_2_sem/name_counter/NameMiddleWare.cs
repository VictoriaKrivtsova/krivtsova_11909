﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2
{
    public class NameMiddleWare
    {
        private readonly RequestDelegate _next;

        public NameMiddleWare(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var name = context.Request.Query["name"];
            if (!String.IsNullOrEmpty(name))
            {
                await context.Response.WriteAsync("Visited: " + name + "\n");

            }
            else
            {
                string form = System.IO.File.ReadAllText("index.html");
                await context.Response.WriteAsync(form);
            }

            await _next.Invoke(context);

        }
    }
}
