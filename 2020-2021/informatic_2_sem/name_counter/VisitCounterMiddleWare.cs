﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2
{
    public class VisitCounterMiddleWare
    {
        private readonly RequestDelegate _next;
        private double counter = -1;

        public VisitCounterMiddleWare(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            counter += 1;
            await context.Response.WriteAsync("Visited: " + counter + "\n");
            await _next.Invoke(context);
        }

    }
}
