﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebApplication2
{
    public class VisitCountMiddleWWare
    {
        private readonly RequestDelegate _next;
        private double count = 0.5;
        private List<string> uniqueUsers;
        public VisitCountMiddleWWare(RequestDelegate next)
        {
            uniqueUsers = new List<string>();
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (!context.Request.Cookies.ContainsKey("logIn"))
            {
                var a = context.Request.Headers["Host"];
                context.Response.Cookies.Append("logIn", "1");
                if (!uniqueUsers.Contains(a))
                {
                    count += 0.5;
                    uniqueUsers.Add(a);
                }
                context.Items.Add("VisitUser", count);
            }

            await _next.Invoke(context);
        }
    }
}
