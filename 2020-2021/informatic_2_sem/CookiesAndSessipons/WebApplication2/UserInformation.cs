﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;

namespace WebApplication2
{
    public class UserInformation
    {
        private readonly RequestDelegate _next;
        private readonly ILoger _loger;
        public static Dictionary<string, int> UserOS;
        public static Dictionary<string, int> UserB;

        public static double VisitCount = 0;
        public UserInformation(RequestDelegate next, ILoger loger)
        {
            UserOS = new Dictionary<string, int>();
            UserB = new Dictionary<string, int>();
            this._next = next;
            this._loger = loger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            await _next.Invoke(context);
            var info = GetUserInfo(context);
            Loging(info);
            _loger.Log(info[0] + " - " + info[1] + "\n");
            if(context.Items["VisitUser"] != null)
            VisitCount = (double)context.Items["VisitUser"];
        }

        private string[] GetUserInfo(HttpContext context)
        {
            var request = context.Request.Headers["User-Agent"].ToString();
            var list = request.Split();

            var oc = String.Concat<char>(list[1].Skip(1));

            var index1 = request.IndexOf("Chrome");
            var browserFull = String.Concat<char>(request.Skip(index1)).Split()[1];
            var index2 = browserFull.IndexOf('/');
            var browser = String.Concat<char>(browserFull.Take(index2));
            return new string[] { oc, browser };
        }

        private void Loging(string[] info)
        {
            if (!UserOS.ContainsKey(info[0]))
                UserOS.Add(info[0], 1);
            else
                UserOS[info[0]]++;

            if (!UserB.ContainsKey(info[1]))
                UserB.Add(info[1], 1);
            else
                UserB[info[1]]++;
        }
    }
}
