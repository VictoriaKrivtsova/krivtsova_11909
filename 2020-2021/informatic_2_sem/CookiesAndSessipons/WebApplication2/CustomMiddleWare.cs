﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;

namespace WebApplication2
{
    public class CustomMiddleWare
    {
        private readonly RequestDelegate _next;
        private string password;
        public CustomMiddleWare(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var session = context.Request.Query["session"];
            password = context.Request.Query["password"];
            if (!String.IsNullOrEmpty(session))
            {
                context.Session.SetString("UserLogIn", "true");
            }

            if (String.IsNullOrEmpty(password))
            {
                var page = System.IO.File.ReadAllText("wwwroot/page/Registration.html");
                await context.Response.WriteAsync(page);
            }

            await _next.Invoke(context);
        }
    }
}
