//1 пункт
	SELECT f.* FROM bookings.flights f
	WHERE f.departure_airport = 'DME' AND (f.actual_departure::DATE = '2016-09-10' OR f.actual_departure::DATE = '2016-09-14') AND f.status = 'Arrived'

//2 пункт
SELECT SPLIT_PART(t.passenger_name, ' ', 2), t.ticket_no, b.book_date FROM bookings.tickets t, bookings.ticket_flights tf, bookings.bookings b,
	(
		SELECT f.flight_id AS f_i, f.actual_departure AS f_ad FROM bookings.flights f
		WHERE date_part('month', f.actual_departure) = '10' AND date_part('year', f.actual_departure) = '2016'
	) AS fi

WHERE t.ticket_no = tf.ticket_no AND tf.flight_id = fi.f_i AND DATE_PART('day', fi.f_ad - b.book_date) > 7

//3 пункт
SELECT f.flight_no, count(*) FROM bookings.flights f
	WHERE date_part('month', f.actual_departure) = '09' AND date_part('year', f.actual_departure) = '2016'
	GROUP BY f.flight_no
	LIMIT 5
//4 пункт
SELECT t.passenger_name, count(*) FROM bookings.tickets t
	JOIN bookings.ticket_flights tf ON tf.ticket_no = t.ticket_no
	JOIN bookings.flights f ON f.flight_id = tf.flight_id
	WHERE f.departure_airport = 'DME'
	GROUP BY t.passenger_name