//пункт 1
WITH RECURSIVE t(curr, iter) AS (
   		 VALUES (1, cast(1 as bigint))
  		UNION ALL
   		 SELECT curr + 1, cast((curr + 1) * iter as bigint)  FROM t WHERE curr < 200
	)
	SELECT * FROM t LIMIT 19;

//пункт 2
CREATE TABLE geo (
    		id int not null primary key,
    		parent_id int references geo(id),
    		name varchar(1000)
	);

	INSERT INTO geo
	(id, parent_id, name)
	VALUES
	(1, null, 'Планета Земля'),
	(2, 1, 'Континент Евразия'),
	(3, 1, 'Континент Северная Америка'),
	(4, 2, 'Европа'),
	(5, 4, 'Россия'),
	(6, 4, 'Германия'),
	(7, 5, 'Москва'),
	(8, 5, 'Санкт-Петербург'),
	(9, 6, 'Берлин');

	WITH RECURSIVE r AS (
	   SELECT id, parent_id, name
	   FROM geo
	   WHERE parent_id = 4

	   UNION

	   SELECT geo.id, geo.parent_id, geo.name
	   FROM geo
	      JOIN r
	          ON geo.parent_id = r.id
	)

SELECT * FROM r;

//пункт 3
	CREATE temp TABLE temp_movies ON COMMIT DROP as
	SELECT m.* FROM public."movies" m
	JOIN public."maleDirectors" male ON male."id" = m."director_id";//для 5 запроса


	CREATE temp TABLE temp_actors ON COMMIT DROP as
	SELECT "actor_id", count(*)
	FROM public."movies_actors"
	GROUP BY "actor_id"
	HAVING count(*) >=3 AND count(*) < 7//для 8 запроса

//пункт 4
//not materilzed
	CREATE VIEW public."maleDirectors"
	 AS
	SELECT d.* from public."directors" d
	JOIN public."movies" m ON d."id" = m."director_id"
	WHERE d."gender" = 'male' AND m."budget" > 1000000;

	ALTER TABLE public."maleDirectors"
	    OWNER TO postgres;
<--при изменении пола, запись с измененными данными удалилась!!-->

	CREATE MATERIALIZED VIEW public."maleDirectors"
		 AS
		SELECT d.* from public."directors" d
		JOIN public."movies" m ON d."id" = m."director_id"
		WHERE d."gender" = 'male' AND m."budget" > 1000000;

		ALTER TABLE public."maleDirectors"
		    OWNER TO postgres;
<--при изменении пола, запись с измененными данными удалилась(при обновлении данных, при простом обновлении "refresh" ничего не происходит)!!-->

//пункт 5
SELECT m.* FROM public."movies" m
	JOIN public."maleDirectors" male ON male."id" = m."director_id"

//пункт 6
SELECT m."year", count(*)
	FROM public."movies" m
	WHERE m."budget" > 1000
	GROUP BY m."year"
	HAVING count(*) = 3

//пункт 7
WITH movies_with_5_actors AS(
	SELECT ma."movie_id"
	FROM public."movies_actors" ma
	JOIN public."movies" m
	ON (m."movie_id" = ma."movie_id")
	GROUP BY (ma."movie_id")
	HAVING (COUNT( ma."movie_id")>2)
	)
	SELECT d."name" FROM public."directors" d, movies_with_5_actors
	WHERE (movies_with_5_actors.movie_id = d."id");

//пункт 8
SELECT "actor_id", count(*)
	FROM public."movies_actors"
	GROUP BY "actor_id"
	HAVING count(*) >=3 AND count(*) < 7