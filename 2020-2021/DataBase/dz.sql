/* ∞ - соединение */

-- 1)

б(year = 2012)movies

SELECT * FROM movies
WHERE year = 2012;

-- 2)

Pi{Prod_name}(Prod ∞ movies)

SELECT Pod_name FROM movies INNER JOIN Prod
ON movies.Prod_id = Prod.id;

-- 3)

Pi{Film_name} (б(rating > 7(movies)) ∞ (б(Country = 'Russia')(Prod)))

SELECT movies.* FROM  movies INNER JOIN Prod
ON movies.Prod_id = Prod.id
WHERE Prod.Country = 'Russia' AND movies.rating > 7;

-- 4)

Pi{Actor_name}(б(Films_count > 10 ^ Country = 'USA')(Y Actor_name, COUNT(*) -> 'Films_count'(movies ∞(movies.Actor_id = Actors.id) Actors)))

SELECT Actors.Actor_name FROM movies INNER JOIN Actors
ON movies.Actor_id = Actors.id
WHERE Actors.Country = 'USA'
GROUP BY (Actor_name)
HAVING COUNT(*) > 10;

-- 5)

(Pi{Prod.name,movies.Genres}(movies ∞(movies.Prod_id = Prod.id) Prod)) % (Genres)

SELECT Prod.name FROM movies INNER JOIN Prod
ON movies.Prod_id = Prod.id
DIVIDE BY Genres.name

-- 6)

Pi{Prod_name}((movies ∞ Prod) % (Pi{movies.year}(б(Actors.name = 'Al Pozino')(movies 8(movies.Actors_id = Actors.id) Actors)))


SELECT Prod.name FROM movies INNER JOIN Prod
ON movies.Prod_id = Prod.id
DIVIDE BY (
SELECT year FROM movies INNER JOIN Actors
ON movies.Actors_id = Actors.id
WHERE Actors.name = 'Al Pozino')

-- 7)

(Pi{Prod.name,movies.Genres}(movies ∞(movies.Prod_id = Prod.id) Prod)) % (б(name != 'Комедия')(Genres))

SELECT Prod.name FROM movies INNER JOIN Prod
ON movies.Prod_id = Prod.id
NOT EXISTS (
SELECT movies.name FROM movies INNER JOIN Genres
ON movies.Genres_id = Genres.id
WHERE movies.genres = 'Комедия'
);


-- 8)
Pi{Actors.name}(б(Actors.Country = 'Italy' ^ Prod.Country = 'France')((Actors 8(Actors.id = movies.Actors_id) Films) ∞(movies.Prod_id = Prod.id) Prod))

SELECT Actors.name FROM Actors INNER JOIN movies
ON Actors.id = movies.Actors_id INNER JOIN Prod
ON movies.Prod_id = Prod.id
WHERE Actors.Country = 'Italy' AND Prod.Country = 'France';

-- 9)

Pi{producers.country}(producers ∞ movies ∞ actors) % б(producers.name = 'Скорсезе') (actors ∞ movies ∞ producers)

SELECT producers.country
FROM producers NATURAL JOIN movies
               NATURAL JOIN actors
DEVIDE BY (
    SELECT actors
    FROM actors NATURAL JOIN movies
                NATURAL JOIN producers
    WHERE producers.name = 'Скорсезе'
)

-- 10)

Pi{movies.Genres,Actors.name}(Actors ∞(Actors.id = movies.Actors_id) movies) %
Pi{Actors.name}((Actors ∞(Actors.id = movies.Actors_id) movies) ∞(movies.Prod_id = Prod.id) (б(Prod.Country = 'Russia')(Prod)))


SELECT movies.Genres FROM movies
WHERE EXISTS(
	SELECT Actors.id FROM movies INNER JOIN Actors
	ON Actors.id = movies.Actors_id INNER JOIN Prod
	ON movies.Prod_id = Prod.id
	WHERE Prod.Country = 'Russia'
);

-- 11) *************************************************

