Пояснение:
V - для любого 
!E - Хотя бы 1
Э - принадлежит

/*пункт 1*/
Find(m.name / m Э movies)
m.year = 2012

SELECT m.name
FROM movies m
WHERE m.year = 2012

/*пункт 2*/
Find(p.name / p Э prod)
V p : (!E m Э movies : (p.id = m.prod_id))

SELECT p.name
FROM prod p
WHERE NOT EXISTS (
SELECT * 
FROM movies m
WHERE p.id = m.prod_id
)

/*пункт 3*/
Find(m.name/m Э movies, p Э prod )
(m.prod_id = p.id & p.Country = "Russia") & (m.rating > 7)

SELECT m.name
FROM movies m, prod p
WHERE (m.prod_id = p.id AND p.Country = "Russia") AND (m.rating > 7)

/*пункт 4*/
Find(p.name/ p Э actors, m Э movies) :
p.Country = "USA" & (!E m : (!E m : m.actors_id = p.actors_id & (!E m : m.actors_id = p.actors_id).id != m.id).id != m.id)

SELECT p.name
FROM actors p, movies m
WHERE p.country = "USA" AND EXISTS (
	SELECT *
	FROM m,p
	WHERE EXISTS (
		SELECT *
		FROM m,p
		WHERE m.actors_id = p.actors_id AND EXISTS(
			SELECT *
			FROM m,p
			WHERE m.actors_id = p.actors_id
		)
	)
)

/*пункт 5*/
Find(p.name / p Э prod)
V g Э genres : (!E m Э movies : (p.id = m.prod_id & m.genres_id = g.id)) 

SELECT p.name 
FROM prod p
WHERE NOT EXISTS (
SELECT *
FROM genres g
WHERE NOT EXISTS(
SELECT *
FROM movies m 
WHERE p.id = m.prod_id AND m.genres_id = g.id
)
);

/*пункт 6*/
Find(p.name / p Э prod, a Э actors, m Э movies)
n Э (m.actors_id = a.id & a.name = "Аль Почино")
V n : (m.prod_id = p.id & n.year = m.year)

SELECT p.name
FROM prod p, actors a, movies m,
(
SELECT *
FROM a Э actors, m Э movies
WHERE m.actors_id = a.id AND a.name = "Аль Почино"
) AS n
WHERE NOT EXISTS (
SELECT *
FROM n
WHERE m.prod_id != p.id OR n.year != m.year
)

/*пункт 7*/
Find(p.name / p Э prod)
V m Э movies (m.prod_id = p.id & m.genres != "Comedy")

SELECT p.name
FROM prod p 
WHERE NOT EXISTS(
SELECT *
FROM movies m 
WHERE m.prod_id != p.id OR m.genres = "Comedy"
);

/*пункт 8*/
Find(a.name / a Э actors, p prod,
n Э (a.Country = "Italy"),
g Э (p.Country = "French")
)
V m Э movies : (m.actors_id = n.id & m.prod_id = g.id)

SELECT a.name
FROM actors a, prod p, (SELECT * FROM a WHERE a.Country = "Italy") AS n,
(SELECT * FROM p WHERE p.Country = "French") AS g
WHERE NOT EXISTS(
SELECT * 
FROM movies m 
WHERE m.actors_id != n.id OR m.prod_id != g.id
);

/*пункт 10*/
Find(m.genres / m Э movies,
p_r Э (p.country = "Russia"),
p Э prod,
a Э actors,
n Э (m.prod_id = p.id & m.actors_id = a.id),
h Э (n.prod_id = p_r.prod_id))
V m : (m.actors_id = h.actors_id)

SELECT m.genres
FROM movies m, prod p, actors a,
(SELECT * FROM prod p WHERE p.country = "Russia") AS p_r,
(SELECT * FROM m, p , a WHERE m.prod_id = p.id AND m.actors_id = a.id) AS n,
(SELECT * FROM n, p_r WHERE n.prod_id = p_r.prod_id) AS h
WHERE NOT EXISTS (
SELECT *  FROM m , h WHERE m.actors_id != h.actors_id)


