//1 пункт
ALTER TABLE public.directors
    ADD PRIMARY KEY (id);
ALTER TABLE public.actors
    ADD PRIMARY KEY (id);

//добавляем режиссера фильма
ALTER TABLE public.movies
    ADD COLUMN director_id integer;
ALTER TABLE public.movies
    ADD CONSTRAINT fk_movie_director FOREIGN KEY (director_id)
    REFERENCES public.directors (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

//добавляем промежуточную таблицу фильм_актер
CREATE TABLE public.movies_actors
(
    movie_id integer,
    actor_id integer
);
ALTER TABLE public.movies_actors
    ADD PRIMARY KEY (movie_id, actor_id);

//добавляем внешние ключи
ALTER TABLE public.movies_actors
    ADD CONSTRAINT fk_actor_movies FOREIGN KEY (actor_id)
    REFERENCES public.actors (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;
ALTER TABLE public.movies_actors
    ADD CONSTRAINT fk_movie_actors FOREIGN KEY (movie_id)
    REFERENCES public.movies (movie_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

//2 пункт//
ALTER TABLE public.directors
    ADD COLUMN best_film_id integer;
ALTER TABLE public.directors
    ADD CONSTRAINT fk_best_movie_id FOREIGN KEY (best_film_id)
    REFERENCES public.movies (movie_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

//3 пункт//
ALTER TABLE movies
DROP CONSTRAINT movies_id;

//добавить новое поле serial move_id//
ALTER TABLE movies
ADD move_id SERIAL CONSTRAINT movies_id PRIMARY KEY;

ALTER TABLE movies
DROP COLUMN id;


//4 пункт//
//изменить default usa на uk//
ALTER TABLE actors
ALTER COLUMN motherland SET DEFAULT ('UK');

//5 пункт//
ALTER TABLE actors
DROP CONSTRAINT actors_number_of_movies_check;

//6 пункт//
ALTER TABLE movies
DROP CONSTRAINT movies_budget_check;

ALTER TABLE movies
ADD CONSTRAINT movies_budget_check Check(budget >= 1000);

//7 пункт//
CREATE TABLE public.genres
(
    id integer NOT NULL,
    name character varying(60),
    PRIMARY KEY (id)
);

ALTER TABLE public.movies DROP COLUMN genres;

CREATE TABLE public.movies_genres
(
    movie_id integer NOT NULL,
    genre_id integer NOT NULL,
    CONSTRAINT movies_genres_pkey PRIMARY KEY (movie_id, genre_id),
    CONSTRAINT fk_genre_movies FOREIGN KEY (genre_id)
        REFERENCES public.genres (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_movie_genres FOREIGN KEY (movie_id)
        REFERENCES public.movies (movie_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

//8 пункт//
CREATE TYPE country AS ENUM ("USA", "UK", "Russia", "France", "Germany");

ALTER TABLE directors
ALTER COLUMN motherland country;

ALTER TABLE actors
ALTER COLUMN motherland country;

//9 пункт//
ALTER TABLE public.actors DROP COLUMN birthday;
ALTER TABLE public.actors
    ADD COLUMN birthday date;
    CONSTRAINT check_if_birthday_after_today CHECK (CURRENT_DATE >= birthday) NOT VALID

//10 пункт//
CREATE OR REPLACE VIEW public.actors_played_after_2000
    AS
    SELECT DISTINCT actors.id,
    actors.surname,
    actors.name,
    actors.motherland,
    actors.number_of_movies,
    actors.birthday
   FROM actors,
    movies_actors,
    movies
  WHERE movies.year > 2000 AND movies_actors.actor_id = actors.id AND movies_actors.movie_id = movies.movie_id
  ORDER BY actors.id;