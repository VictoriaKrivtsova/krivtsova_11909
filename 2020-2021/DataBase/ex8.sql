//1 пункт
SELECT * FROM public."directors"
	SELECT * FROM public."movies"

	SELECT * FROM public."directors" d
	JOIN public."movies" m ON d."best_film_id" = m."movie_id"
	WHERE m."year" = 2000

//2 пункт
SELECT *,
	(SELECT count(*) FROM public."movies" m
	WHERE d."id" = m."director_id") AS filmsCount
	FROM public."directors" d
	HAVING filmsCount > 5

//3 пункт
SELECT "movie_id", count(*)
	FROM public."movies_actors"
	GROUP BY "movie_id"
	HAVING count(*) > 10

//4 пункт
ALTER TABLE movies
	ADD rating INTEGER NULL;

	SELECT * FROM public."movies"
	WHERE "country" = 'USA'
	ORDER BY "rating" DESC
	limit 10


//5 пункт
SELECT * FROM public."movies" m WHERE m."movie_id" IN
	(SELECT distinct mg."movie_id" FROM public."movies_genres" mg
	JOIN public."genres" g ON mg."genre_id" = g."id"
	WHERE g."name" = 'horror'
	AND exists(SELECT 1 FROM public."movies_actors" ma
		  	JOIN public."actors" a ON ma."actor_id" = a."id"
		  	AND a."motherland" = 'UK'))

//6 пункт
SELECT * FROM public."actors" a
	WHERE exists(
		SELECT 1 FROM public."movies_actors" ma
		JOIN public."movies" m ON m."movie_id" = ma."movie_id"
		JOIN public."directors" d ON m."director_id" = d."id"
		WHERE d."motherland" = 'UK'
		AND m."year" >= 2007 AND m."year" <= 2010
		AND a."id" = ma."actor_id")

//7 пункт
SELECT CASE WHEN m."year" < 2000 THEN '< 2000'
			WHEN m."year" BETWEEN 2000 AND 2005 THEN '2000-2005'
			WHEN m."year" BETWEEN 2005 AND 2010 THEN '2005-2010'
			WHEN m."year" > 2010 THEN '> 2010'
		END AS dateRange,
	AVG(m."budget") FROM public."movies" m
	GROUP BY dateRange;

//8 пункт
SELECT SUM(m."budget") FROM public."movies" m
	JOIN public."directors" d ON d."id" = m."director_id"
	WHERE d."surname" LIKE '%n'OR d."surname" LIKE '%v'

//9 пункт
SELECT m."year", MAX(m."budget")
	FROM public."movies" m
	GROUP BY m."year"

//10 пункт
SELECT m."name" FROM public."movies" m,
	(
	SELECT MIN(m."budget") AS max_for_movies_before_2010 FROM m
	WHERE (m."year" >= 2010)
	) AS budget
	WHERE (m."year" < 2010 AND m."budget" < budget.max_for_movies_before_2010);

//11 пункт
SELECT d."name" FROM public."directors" d, public."movies" m,
	(
	/*минимум бюджета за 2015*/
	SELECT MIN(m."budget") AS min FROM public."movies" m
	WHERE (m."year" = 2015)
	) AS year_2015,
	(
	SELECT MAX(m."budget") AS max FROM public."movies" m
	WHERE (m."year" = 2016)
	) AS year_2016
	WHERE (d."id" = m."director_id"
	AND m."budget" > year_2015.min
	AND m."budget" <year_2016.max );