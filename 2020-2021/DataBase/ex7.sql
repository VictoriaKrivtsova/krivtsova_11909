//1 пункт//
SELECT * FROM PUBLIC."directors"
SELECT * FROM PUBLIC."movies"

SELECT * FROM PUBLIC."directors" d
JOIN PUBLIC."movies" m ON d."best_film_id" = m."movie_id"
WHERE m."year" = 2000

//2 пункт//
SELECT *,
(SELECT count(*) FROM PUBLIC."movies" m
WHERE d."id" = m."director_id") AS filmsCount
FROM PUBLIC."directors" d
WHERE filmsCount > 5

//3 пункт
SELECT "movie_id", count(*)
FROM PUBLIC."movies_actors"
GROUP BY "movie_id"
HAVING count(*) > 10

//4 пункт//
SELECT * FROM PUBLIC."movies"
WHERE "country" = 'USA'
ORDER BY "rating" DESC
LIMIT 10

//5 пункт
SELECT * FROM PUBLIC."movies" m WHERE m."movie_id" IN
(SELECT DISTINCT mg."movie_id" FROM PUBLIC."movies_genres" mg
JOIN PUBLIC."genres" g ON mg."genre_id" = g."id"
WHERE g."name" = 'horror'
AND exists(SELECT 1 FROM PUBLIC."movies_actors" ma
	  	JOIN PUBLIC."actors" a ON ma."actor_id" = a."id"
	  	AND a."motherland" = 'UK'))

//6 пункт
SELECT * FROM movies
ORDER BY movies.duration DESC
OFFSET 4 ROWS
FETCH FIRST 4 ROW ONLY;

//7 пункт
SELECT movies_genres.genre FROM movies_genres
GROUP BY genre HAVING COUNT (genre) > 1;

//8 пункт
SELECT genres.name
FROM genres, actors, movies, movies_actors, movies_genres
WHERE (actors.motherland = 'France' OR actors.motherland = 'London') AND genres.id = movies_genres.genre_id AND movies_genres.movie_id = movies.movie_id AND  movies.movie_id = movies_actors.movie_id AND movies_actors.actor_id = actors.id