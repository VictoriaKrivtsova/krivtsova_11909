CREATE TABLE movies
(
	name CHARACTER VARYING(100),
	description CHARACTER VARYING(1000),
	year INTEGER CHECK(year > 1900 AND year < 2030),
	genres CHARACTER VARYING(1000),
	country CHARACTER VARYING(200),
	budget INTEGER CHECK(budget > 10000),
	CONSTRAINT name_year PRIMARY KEY(name, year)
);

CREATE TABLE actors
(
    id SERIAL,
	surname CHARACTER VARYING(100) UNIQUE,
	name CHARACTER VARYING(1000) UNIQUE,
	birthday CHARACTER VARYING(10) UNIQUE,
	motherland CHARACTER VARYING(1000) default('Usa'),
	number_of_movies INTEGER CHECK(number_of_movies >5)
);

CREATE TABLE producer
(
    id SERIAL,
	surname CHARACTER VARYING(100),
	name CHARACTER VARYING(1000),
	birthday CHARACTER VARYING(10),
	motherland CHARACTER VARYING(1000) default('Usa')
);


INSERT INTO actors
VALUES
(1,	'Valiulin',	'Damir', '25/07/1990', 'Australia', 100),
(2, 'Gabdulhakov', 'Kerim', '19/06/1989', 'Germany', 100),
(3, 'Saidashev', 'Bulat', '9/05/2000', 'Russia', 100),
(4, 'Merkulov', 'Danil', '09/04/1996', 'Russia', 12),
(5, 'Sharafutdinov', 'Karim', '02/01/2001', 'Tatarstan', 500);

INSERT INTO actors(id, surname, name, birthday, number_of_movies)
VALUES
(6, 'Kinshurin', 'Marat', '18/10/1998', 10);


INSERT INTO producer(id, surname, name, birthday)
VALUES
(1, 'Valiulin"', 'Bulat', '28/08/1978');

INSERT INTO producer
VALUES
(2,	'Gabdulhakov',	'Damir', '16/01/1983', 'Germany'),
(3, 'Saidashev', 'Bulat', '07/09/1987', 'Russia'),
(4, 'Sharafutdinov', 'Danil', '10/10/2000', 'Nitherland'),
(5, 'Merkulov', 'Karim', '09/04/1996', 'Tatarstan'),
(6, 'Chesnokov', 'Vlad', '18/10/1998', 'Usa');

INSERT INTO movies
VALUES
('Dark', 'very interesting film', 1905, 'horror, thriller','',10005),
('Shark', 'very interesting film', 1957, 'horror, fantasy', 'Kanada',17398),
('Madagaskar', 'VERY interesting film', 2005, 'horror, drama', 'Russia',573937),
('Batman', 'the worst film ever', 2014, 'dramma, comedy', 'Germany',38365),
('Laugh', 'not interesting film', 2010, 'comedy', 'Sweden',25753),
('Lol', 'very interesting film', 2020, 'comedy, thriller', 'Australia',563846);

ALTER TABLE movies
DROP CONSTRAINT name_year;

ALTER TABLE movies
ADD id SERIAL CONSTRAINT movies_id PRIMARY KEY;
