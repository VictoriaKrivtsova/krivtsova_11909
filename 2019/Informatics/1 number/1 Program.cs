﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_number
{
    class Program
    {
        static void Main(string[] args)
        {
            var ticket = Convert.ToInt32(Console.ReadLine());
            var previousTicket = ticket - 1;
            var nextTicket = ticket + 1;
            int[] array1 = new int[6];
            int[] array2 = new int[6];
            var check = 0;
            var check2 = 0;
            for (int i = 0; i < 6; i++)
            {
                array1[i] = previousTicket % 10;
                previousTicket /= 10;
            }
            for (int i = 0; i < 6; i++)
            {
                array2[i] = nextTicket % 10;
                nextTicket /= 10;
            }
            if (array1[0] + array1[2] + array1[4] == array1[1] + array1[3] + array1[5])
                check++;
            if (array2[0] + array2[2] + array2[4] == array2[1] + array2[3] + array2[5])
                check2++;
            if (check > 0 || check2 > 0)
                Console.WriteLine("True");
            Console.WriteLine("False");
        }
    }
}
