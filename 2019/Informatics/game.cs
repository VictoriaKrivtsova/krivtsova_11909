﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    public class Cards
    {
        public char[] card = new char[12] { '2', '3', '4', '5', '6', '7', '8', '9', 'V', 'D', 'K', 'T' };
        public int[] value = new int[12] {2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 11};

    }

    public class User
    {
        public int cardUser()
        {
            int exit = 0;
            string a;
            var cards = new Cards();
            int sum = 0;
            var t;
            var rand = new Random();
            do
            {
                t = rand.Next(0, 11);
                Console.WriteLine("Your card is:{0}", cards.card[t]);
                sum += cards.value[t];
                Console.WriteLine("another card? : press f");
                a = Console.ReadLine();
            } while (a == "f");
            exit = -1;
            return sum;
        }
    }

    public class Computer
    {
        public int cardComputer()
        {
            int exit = 0;
            int sum;
            //ход компьютера;
            exit = -1;
            return sum;
        }
    }
    public class game
    {
        static void Main(string[] args)
        {
            var sum_User = new User();
            var sum_Computer = new Computer();
            var userSum = sum_User.cardUser();
            var computerSum = sum_Computer.cardComputer();

            //цикл, в котором игроки будут ходить по очереди.

            if (userSum <= 21 || userSum > computerSum || computerSum > 21)
                Console.WriteLine("You win!!");
            else Console.WriteLine("You die!!");
        }
    }
}
